# inject-docs

This repository contains the documentation for the inject exercise platform.

## Table of Contents

- [Dependencies](#dependencies)
- [Usage (Local Server)](#usage-local-server)
- [Build GitLab Pages](#build-gitlab-pages)
- [Files Pulled from Other Repositories](#files-pulled-from-other-repositories)
- [Pipeline and Deployment](#pipeline-and-deployment)

## Dependencies

- [Python](https://www.python.org/downloads/) version 3.10
- [Poetry](https://python-poetry.org/docs/) version 1.8.0 and higher

## Usage (local server)

To set up and run a local server for the documentation:

1. Install dependencies:
   ```bash
   poetry install
   ```

2. Run the utilities script to set up the necessary environment:
   ```bash
   poetry run ./utilities.sh
   ```

3. Start the local MkDocs server:
   ```bash
   poetry run mkdocs serve
   ```

4. Access the documentation at [http://localhost:8000](http://localhost:8000). If port 8000 is taken by another service, you can change the default port using the `-a` or `--dev-addr` option:
   ```bash
   poetry run mkdocs serve -a <IP:PORT>
   ```

## Build GitLab Pages

The documentation is built and deployed to GitLab Pages. The hosted documentation is available at:

[https://inject.pages.fi.muni.cz/inject-docs](https://inject.pages.fi.muni.cz/inject-docs)

## Files Pulled from Other Repositories

The `utilities.sh` script pulls several essential files from other repositories to ensure centralized updates. These files include:

- `backend/definitions/README.md`: Documentation on the definition files structure.
- `backend/openapi.yml`: OpenAPI documentation for the API.
- `frontend/docker/nginx-deployment/README.md`: Documentation on the Nginx deployment setup.
- `showcase-definition/assets/definition.zip`: A showcase definition file.

Additionally, the GitLab pipeline pulls and zips the following files:

- `frontend/docker/nginx-deployment`: This directory is zipped (excluding `compose-from-monorepo.yml`) and saved as `deployment-files.zip` in the `files-from-repos` directory.

## Pipeline and Deployment

Deployment of the documentation to GitLab Pages is managed through a CI/CD pipeline defined in the .gitlab-ci.yml file. GitLab CI/CD will automatically run the pipeline, which includes the following stages. You can also manually trigger the pipeline if needed:

* Setup: Install necessary dependencies and run the utilities.sh script to set up directories and copy files.
* Commit Zip: Optionally commit the zipped deployment files and showcase definition to the repository.
* Build Documentation: Use MkDocs to build the site.
* Deploy Pages: Deploy the built documentation to GitLab Pages.

**Important**: Certain parts of the pipeline, including the commit and deploy stages, are only run on the `main` branch. This ensures that only finalized and approved changes are deployed to the live documentation site.

The pipeline will automatically build and deploy the documentation to GitLab Pages upon successful completion of all stages.

## Versioning

The documentation supports the deployment of multiple pages for different versions. This is done via parallel deployments. 
The way these parallel pages are to be deployed is as follows. 
The main branch of the project is always the newest, not-yet-published page. Here we can slowly keep adding more and more content. 
This branch should never be deployed. Only upon new release can we deploy this new page. 
For that, we are going to create a new branch named after the release name, and this new branch is what we are going to release. 
**The main page is always the newest published branch.**

The main difference between the main page of docs and paraller is that paraller has a postfix in the URL and is thus not the primary site. 
When setting up the version, you need to make a few adjustments to the gitlab-ci.yml.

### 1. Parallel page setup from the main page

In the file `gitlab-ci.yml` the following lines need to be added under the `pages` stage when creating a new parallel page from the main page:
```
  pages:
    path_prefix: "$CI_PIPELINE_ID"
    expire_in: never
```

This means that this new page will have a postfix in the link that is the same as the name of the release. 
This means that a new page will be generated under the URL: https://inject.pages.fi.muni.cz/{release-name}

You also need to specify the expiration time of this deployment since, by default, they are set to expire in a set amount of time.

### 2. Setup shared for all versions

Another part that needs to be adjusted for each page upon new release is the menu for switching versions. 
It's located in `overrides/partials/header.html`. Here you can find the lines:
```
 <!-- Dropdown Menu for Page Versions -->
 <div class="md-header__dropdown">
 <select onchange="window.location.href=this.value;">
 <option value="#">Citadel</option>
 <option value="/bastion/">Bastion</option>
 <option value="/aegis/">Aegis</option>
 </select>
 </div>
```

On a parallel page, it looks like:
```
 <!-- Dropdown Menu for Page Versions -->
 <div class="md-header__dropdown">
 <select onchange="window.location.href=this.value;">
 <option value="#">Bastion</option>
 <option value="/">Citadel</option>
 <option value="/aegis/">Aegis</option>
 </select>
 </div>
```

When adding a new page, you will need to add a new option to this list. A few things to mention are:
* value="#" points to the page where the user is currently
* value="/" points to the main page from the parallel page
* value="/{branch-name}/" points to a parallel page

### 3. Finishing touches

To avoid accidental publishing, the tag `only:` should always be set to the name of the branch where it is located. 
Also, the main branch should never have a valid gitlab-ci.yml that can publish pages. 
For that reason the tag `only:` on the main branch of the project should be set to a different name other than the main branch.
On the main branch, the tag is set to `change-me`, you need to change all of the occurrences of this tag to the current branch name.

Then, set up the new branch as protected.

Finally, update the news box on the index page to reflect the current progress on the platform. An update also the news box in the older versions as well, and include a link to the latest one.