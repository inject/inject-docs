---
hide:
  - navigation
  - toc
---
<img class="main-logo" alt="" src="images/inject-logo--horizontal-yellow.svg"> 
# INJECT Exercise Platform Documentation

<div class="bigger-font-size" markdown>
Welcome! Whether you're responsible for technical deployment or facilitating tabletop exercises, this documentation will equip you with the tools and knowledge needed for successful implementation.
</div>


## What is the INJECT Exercise Platform (IXP)?
- An open-source digital platform designed for conducting interactive tabletop exercises.
- Provides immediate reactions to decisions, enhancing the learning process.
- Simplifies the preparation, execution, and evaluation of exercises, reducing organizer workload.

## Quick Navigation 

<div class="main-gridc" markdown>
<div class="navigation-main" markdown>
  [Installation](tech/installation/overview.md){ .md-button }
</div>
<div class="navigation-main" markdown>
  [Exercise Preparation](INJECT_process/intro/overview.md){ .md-button }
</div>
<div class="navigation-main" markdown>
  [Report issues](report-issue.md){ .md-button }
</div>
</div>

!!! News
    *February 16, 2025* 
    We've just released the third version of the INJECT Exercise Platform (v 3.0.0). The entire documentation has been updated to reflect this new version. 
    If you are using an older version of the platform, please check our [Version Compatibility](tech/version-compatibility.md) page.
 

## The Big Picture

<div class="main_grid-border" markdown>

<div class="main_grid-cads" markdown>
<div class="grid cards" markdown>

-   ### Technical Documentation

    ---
    **The technical documentation provides insight into the platform’s architecture, APIs, security features, and installation process.**
    
    <br>

    **[Installation Guide ↲](tech/installation/overview.md)**:
      Step-by-step instructions for deploying IXP using Docker Compose and Nginx.
    **[Architecture Overview](tech/architecture/overview.md)**:
      Detailed documentation on the architecture of the IXP, including component definition.  
    **[Definition Description](tech/architecture/definitions/README.md)**: documentation of exercise definitions - learn how YAML-based configurations describe the structure and content of exercises.
    **[Version Compatibility](tech/version-compatibility.md)**: Overview of platform versions and their corresponding documentation versions - check which documentation version matches your platform version.
    **[Security](tech/security.md)**:
      Details on the security measures and practices implemented in the IXP.  
    **[API Documentation](tech/api/overview.md)**:
      Comprehensive documentation of the APIs provided by IXP, including RESTful APIs.  
  

-   ### Exercise Preparation Guide

    ---
    **The INJECT Process provides a clear guide on how to plan, run, and evaluate tabletop exercises using the IXP platform.**

    <br>
  
    **[Start Here ↲](INJECT_process/intro/overview.md):**  If you are new to IXP, this is the best place to begin.
    
    **[01 Understanding Phase](INJECT_process/understand/overview.md):** 
    Gather the key information about the exercise’s goals, scope, and resources.

    **[02 Specification Phase](INJECT_process/specify/overview.md):** 
    Specify the exercise learning objectives, activities and scenario.

    **[03 Preparation Phase](INJECT_process/prepare/overview.md):** 
    Get IXP content, logistics, and resources ready for the exercise.

    **[04 Execution Phase](INJECT_process/execute/overview.md):** 
    Run the exercise and manage all activities.

    **[05 Reflection Phase](INJECT_process/reflect/overview.md):** 
    Review the exercise and identify improvements.


-   ### Additional Resources

    ---

    **[Glossary of Terms](glossary-of-terms.md)**: Definitions of key terms and concepts used in the IXP.  
    **[Report Issue](report-issue.md)**: Instructions for reporting bugs, issues, or providing feedback on the IXP.  
    **[Known Issues](known-issues.md)**: A list of known issues and how to fix them.  
    **[Acknowledgements](acknowledgements.md)**: Recognition of individuals or organizations that have contributed to the development or support of the IXP.  
    **[Authors](authors.md)**: Meet the team behind the development of IXP.

-   ### Get in Touch
    ---
    We believe that collaboration helps us learn from one another. If you have questions about IXP or would like to discuss potential collaborations, please feel free to reach out through our website.

    <br>

    <div class="navigation" markdown>
      [INJECT Website](https://inject.muni.cz){ .md-button }
    </div>

</div>
</div>
</div>

