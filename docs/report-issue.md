If you encounter any issues or bugs while using the INJECT Exercise Platform (IXP), please follow the instructions below to report them.
Before submitting a bug report, we recommend checking the [Known Issues](known-issues.md) page to see if the issue has already been identified and if there are any available fixes.

* Open your email client and create a new email.
* Set the recipient email address to [our adress](mailto:git+inject-inject-issues-30987-issue-@gitlab.fi.muni.cz).
* In the subject line, briefly describe the nature of your inquiry (e.g., Bug Report: Application Crashes).
* In the body of the email, provide detailed information about the issue you are experiencing.
  Include steps to reproduce the problem, screenshots (if applicable), and any other relevant details.
* Also make sure to include the backend and frontend versions, which you can find in the Exercise Panel.
* Once you have filled out all the necessary information, click the send button to dispatch your email to us.

Project maintainers may reach out to you to gather more information or provide updates on the status of your inquiry.
Respond to any communications from project maintainers directly through email.

**Additional Tips**

* Be as descriptive as possible when outlining the issue in your email.
  Clear and detailed descriptions help project maintainers understand and address the problem more effectively.
* Include any relevant attachments, such as log files or error messages, to assist project maintainers in diagnosing the issue.
* Check your email regularly for any follow-up communications from project maintainers regarding your inquiry.

By following these steps, you can easily report bugs or provide feedback to project maintainers.
Your contributions help improve the overall quality and functionality of the project for all users.
