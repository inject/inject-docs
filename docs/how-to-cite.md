When referencing the INJECT Exercise Platform, please cite our paper titled:
[From Paper to Platform: Evolution of a Novel Learning Environment for Tabletop Exercises](https://www.muni.cz/en/research/publications/2390899).

This paper provides insights and practical experiences derived from a cybersecurity course where
tabletop exercises were introduced utilizing the innovative technology of the INJECT Exercise Platform (IXP).

When citing this paper please use this BibTeX entry:
<div class="no-bcg">
``` { . .copy}
@inproceedings{Svabensky2024FromPaperToPlatform,
    author    = {\v{S}v\'{a}bensk\'{y}, Valdemar and Vykopal, Jan and Hor\'{a}k, Martin and Hofbauer, Martin and \v{C}eleda, Pavel},
    title     = {From Paper to Platform: Evolution of a Novel Learning Environment for Tabletop Exercises},
    year      = {2024},
    isbn      = {9798400706004},
    publisher = {Association for Computing Machinery},
    address   = {New York, NY, USA},
    url       = {https://doi.org/10.1145/3649217.3653639},
    doi       = {10.1145/3649217.3653639},
    booktitle = {Proceedings of the 2024 on Innovation and Technology in Computer Science Education V. 1},
    pages     = {213–219},
    numpages  = {7},
    location  = {Milan, Italy},
    series    = {ITiCSE 2024}
}
```
</div>

For a comprehensive overview of tabletop exercises, we recommend our systematic literature review:
[Research and Practice of Delivering Tabletop Exercises](https://www.muni.cz/en/research/publications/2390898).

When citing this paper please use this BibTeX entry:
<div class="no-bcg">
``` { . .copy }
@inproceedings{Vykopal2024ResearchAndPractice,
    author    = {Vykopal, Jan and \v{C}eleda, Pavel and \v{S}v\'{a}bensk\'{y}, Valdemar and Hofbauer, Martin and Hor\'{a}k, Martin},
    title     = {Research and Practice of Delivering Tabletop Exercises},
    year      = {2024},
    isbn      = {9798400706004},
    publisher = {Association for Computing Machinery},
    address   = {New York, NY, USA},
    url       = {https://doi.org/10.1145/3649217.3653642},
    doi       = {10.1145/3649217.3653642},
    booktitle = {Proceedings of the 2024 on Innovation and Technology in Computer Science Education V. 1},
    pages     = {220–226},
    numpages  = {7},
    location  = {Milan, Italy},
    series    = {ITiCSE 2024}
}
```
</div>
