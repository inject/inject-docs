# How to upgrade the definition version?

If you want to upgrade the current definition to a new version, use the following [CHANGELOG](CHANGELOG.md).

## How to proceed:  

1. Take your current definition, and in `config.yml`, check the version.  

2. Change the version in `config.yml` to a new version.

3. Use the [CHANGELOG](CHANGELOG.md) and start making changes step by step from your current version to the newest version that is compatible with IXP.  

4. Make one single change at a time.  

5. Be sure to make the change for all the concerning files and their attributes before proceeding with the following change. 

6. Each version contains information about changes and a structured description of affected files and their arguments. 

7. Note that not every change is reflected in the files; some changes are only concerned with the behavior of certain definition aspects rather than syntax changes in the YAML files. 

8. Lastly, check the new version against [definition documentation](README.md) to see if the arguments in the files are the same. 

9. Zip the definition and upload it to the IXP for the last validation.  

10. **Done.** Enjoy your upgraded version!

## Example:

* Current version: **0.12.1**
* New version: **0.18.1**

* In the `config.yml` change the field `version` from 0.12.1 to 0.18.1.

### 0.12.1 to 0.13.0
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.13.0**.
2. The only change is transforming the `control` field into `controls` in `questionnaires.yml` in questions.
3. Open `questionnaires.yml`. If the `control` field is present, change it to `controls` in every question. 
4. The change enhances the control options, as shown in the [definition documentation](README.md). To keep the functionality the same, prepend the previous control with the number of correct answer with the colon, e.g. correct answer is 1, prepend `1: `
5. Save the file.
6. Check the `questionnaires.yml` against the `questionnaires.yml` structure in the [definition documentation](README.md).

### 0.13.0 to 0.14.0
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.14.0**.
2. The only change is allowing more `info` type channels. 
3. Open `injects.yml` and add to every info type inject field `target` with the name of the current info type channel specified in `channels.yml`.
4. Save the file.
5. Check the `injects.yml` against the `injects.yml` structure in the [definition documentation](README.md).

### 0.14.0 to 0.15.0
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.15.0**.
2. The change lies in adding a new type of questionnaire, free-form. The new feature does not alter the current definition syntax.
3. Skip the change.

### 0.15.0 to 0.15.1
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.15.1**.
2. The change lies in the addition of confirmation buttons to injects. It is only a new feature that does not alter the current definition syntax.
3. Skip the change.

### 0.15.1 to 0.15.2
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.15.2**.
2. The change contains adding the field `description` to `config.yml`, `objectives.yml`, `channels.yml`, `milestones.yml`, `roles.yml` and `questionnaires.yml`. As seen in the [definition documentation](README.md), the fields are optional, so it is up to the designers to include more information in the definition.
3. Another change is addition of filed `display_name` to `channels.yml`, `milestones.yml` and `roles.yml`. Again, this new field is set to the previously set name, meaning the designer chooses to improve the definition.
4. Check the affected files against the structure in the [definition documentation](README.md).

### 0.15.2 to 0.16.0
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.16.0**.
2. First, optional fields `target_audience` and `prerequisites` are added to the `config.yml`. The designer can choose to add this information.
3. Next, the `subject` field is added to the email templates in the `emails.yml`. Add the field `subject` to every email template with a meaningful subject.
4. Save the files.
5. Check the `emails.yml` against the `emails.yml` structure in the [definition documentation](README.md).
6. Check the `config.yml` against the `config.yml` structure in the [definition documentation](README.md).
7. The significant change lies in changing how the learning activities are bound together with milestones. Now the milestones are written to the `objectives.yml` instead of activities being written to the `milestones.yml`.
8. Add field `milestones` to every activity, with the name of the milestones previously bearing the name of the activity.
9. After connecting all activities with milestones, open `milestones.yml` and remove every field `activity`.
10. Check the `milestones.yml` against the `milestones.yml` structure in the [definition documentation](README.md).
11. Check the `objectives.yml` against the `objectives.yml` structure in the [definition documentation](README.md).

### 0.16.0 to 0.17.0
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.17.0**.
2. The change lies in adding the field `category` to `tools.yml`. If you previously used a category in `tools.yml` with the notation **category_toolName**, it is now mandatory to write the category in a separate field.
3. Add the field `category` to every tool with a category, remove the category from the name, and write it to the new field.
4. The next change is removing the field `description` from `questionnaires.yml`. Open `questionnaires.yml` and remove the field description from every questionnaire.
5. The last change is adding the new field `content` to the `questionnaires.yml`. The change allows for more information to be embedded in questionnaires. You can skip this change.
6. Save the files.
7. Check the `tools.yml` against the `tools.yml` structure in the [definition documentation](README.md).
8. Check the `questionnaires.yml` against the `questionnaires.yml` structure in the [definition documentation](README.md).

### 0.17.0 to 0.17.1
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.17.1**.
2. The change lies in adding embedding options into content fields. The change does not alter the current definition.
3. Skip the change.

### 0.17.1 to 0.17.2
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.17.2**.
2. Fixed embedding in email templates. No change is required.
3. Skip the change.

### 0.17.2 to 0.18.0
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.18.0**.
2. The first change is adding the `content` field to `default_response` in tools.
3. Fix `default_response` according to [definition documentation](README.md).
4. Check the `tools.yml` against the `tools.yml` structure in the [definition documentation](README.md).
5. The second change lies in adding the optional field `requires_input` to `tools.yml`. This new feature allows you to use tools without input parameters. So, it is up to the designer if such a change is desired.
6. Otherwise, skip the change.

### 0.18.0 to 0.18.1
1. In the [CHANGELOG](CHANGELOG.md), find the first subsequent definition version to the current version, which is, in this case, **0.18.1**.
2. The change lies in adding an optional `note` to `questionnaires.yml`. This feature only adds additional information to questionnaires, so it is up to the designer.
3. Otherwise, skip the change.

* Create a new .zip archive and upload it to the IXP. 
* The upload process will check the definition for errors.

**You have successfully upgraded the definition version.**
