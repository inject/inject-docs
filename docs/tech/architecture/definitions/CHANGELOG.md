## 0.18.1

Add note to questions.

### questionnaires.yml

- add optional field `note` to question


## 0.18.0

Add option to tools to signify that they don't require an input.
Change the type of `default_response` field.

### tools.yml

- add optional field `requires_input` to tool
- change the type of `default_response` to content


## 0.17.2

Fixed embedding in email templates

No format changes


## 0.17.1

Allow embedding images and videos.

All `content` type fields now allow for embedding images/audio/video by using special syntax.
See readme for more information.

## 0.17.0

Add support for markdown in questionnaires.
Add missing field from tools.

### questionnaires.yml

- remove field `description` from questionnaire
- add field `content` to questionnaire

### tools.yml

- add field `category` to tool


## 0.16.0

More definition enhancements.

### config.yml

- add field `target_audience`
- add field `prerequisites`

### objectives.yml

- add field `order` to objectives
- add field `milestones` to activities

### milestones.yml

- remove field `activity`

### email.yml

- add field `subject` to email templates

## 0.15.2

Allow exercise designers to add more information to the definition.

### config.yml

- add field `description`

### objectives.yml

- add field `description` to both objectives and activities

### channels.yml

- add field `display_name`
- add field `description`

### milestones.yml

- add field `display_name`
- add field `description`
- add field `tags`

### roles.yml

- add field `display_name`
- add field `description`

### questionnaires.yml

- add field `description` to questionnaire

## 0.15.1

Add confirmation button to info alternatives.

### injects.yml

- add field `confirmation` to info alternatives

## 0.15.0

Allow specifying open-ended questions in questionnaires.

### questionnaires.yml

- add field `type` to questions
- add field `related_milestones` to `free-form` type questions 
- add field `multiline` to `free-form` type questions

## 0.14.0

Allow multiple info channels in a definition.

### injects.yml

- add field `target` that specifies info channel for the info injects

### channels.yml
- allow occurrence of multiple `info` channels

## 0.13.0

Enabled a more fine-grained control over milestone modifications for 
scale-based questions.

### questionnaires.yml

- remove field `control` in question objects
- added field `controls` in question objects, 
  which is a mapping of `choice number` to `control`.
  See definition documentation for more details.

## 0.12.1

Enable specification of `roles` in `email` type injects.

### injects.yml

- allow specifying `roles` in control blocks inside `email` type injects

## 0.12.0

Replace `text` field by `content` in `Questionnaire` questions.
From now, the questions of questionnaires can be formatted via `content`.

### questionnaires.yml

- remove field `text` of questions
- add field `content` to questions

## 0.11.0

Change the behavior of `delay` on injects.
Whenever an alternative is delayed, the condition is now checked throughout the duration.
If the condition becomes false, the inject is cancelled and a new alternative can be selected.

### injects.yml

- change behavior of `delay` field

## 0.10.0

Change the behavior of `info` alternatives.
When an alternative contains no `content`, it will not create an action log.

### injects.yml

- change behavior of `content` field in `info` injects

## 0.9.1

Add option to specify initial state for milestones.

### milestones.yml

- add optional field `initial_state`


## 0.9.0

Removed manual injects.
All injects are now considered to be **automatic injects**.

### injects.yml

- remove field `auto`


## 0.8.0

Add a new required concept called `learning objectives`. 
This concept _must_ be included in all exercise definition.

### objectives.yml

- added new file

### milestones.yml

- add field `activity`

## 0.7.0

Add simple questionnaires.

### channels.yml

- add new channel type `form`

### questionnaires.yml

- added new file


## 0.6.1

Add overlay to injects.

### injects.yml

- add field `overlay` to `info` and `email` injects


## 0.6.0

Added a new concept called `channels`. This concept is saved in a new `channels.yml` file.
See definition documentation for details.

### config.yml

- remove `enable_email` field
- remove `team_visible_milestones` field, team visible milestones can now be specified without this flag

### channels.yml

- added this new required file

### injects.yml

- remove field `hidden`
- rename field `injects` to `alternatives`
- add field `type` to inject
- specify new types of alternatives, see definition documentation for details

### tools.yml

- remove old `content`, `content_path`, `file_name` fields
- remove old `milestone_condition`, `activate_milestone`, `deactivate_milestone`, `roles` fields
- add new fields `content` and `control`, see definition documentation for details

### email.yml

- remove `activate_milestone`, `deactivate_milestone` fields from email address
- add `control` field to email address
- remove old `content`, `content_path`, `file_name` templates from email templates
- remove `activate_milestone`, `deactivate_milestone` fields from templates
- add new fields `content` and `control` to email templates,
  see definition documentation for details
 

## 0.5.1

Allow using directories of files instead of single YAML files inside the definition.

## 0.5.0

### injects.yml

- add inject field `subject`

## 0.4.1

### config.yml

- remove `team_file_upload` field, the feature is now always enabled
- remove `custom_team_names` field, team names have been removed from the definition, it is now
  a parameter when creating an exercise
- remove `team_count` field, it is now a parameter when creating an exercise

### teams.yml

- temporarily removed from the definition

## 0.4.0

Introduce simplified syntax for `activate_milestone` and `deactivate_milestone`,
which replaced the old field `reach_milestone`.

Instead of the old syntax from `reach_milestone` reminding logical condition,
the new fields `(de)activate_milestone` have a simple enumeration of milestones,
which could be separated by a comma, space, or comma followed by space.

### injects.yml

- inject field `reach_milestone` is replaced by fields `activate_milestone` and `deactivate_milestone`

### tools.yml

- response field `reach_milestone` is replaced by fields `activate_milestone` and `deactivate_milestone`

### email.yml

- email and template field `reach_milestone` is replaced by fields `activate_milestone` and `deactivate_milestone`

## 0.3.0

### milestones.yml

- add field `final` to milestones

## 0.2.0

Introduce support for markdown content in `injects`, `tool responses` and `email templates`.

Add new optional folder `content` to the definition file structure. 
This folder should contain markdown files which will be pointed to by `content_path` fields.
Only one of the `content`, `content_path` fields can be set at the same time. 
If neither field is set, then the content is considered to be empty.

### tools.yml

- rename response field `answer` to `content`
- add field `content_path` to responses

### inject.yml

- add field `content_path` to injects

### email.yml

- add field `content_path` to emails

## 0.1.1

### email.yml

- Add `organization` field to email addresses

### injects.yml

- Add `organization` field to inject categories

## 0.1.0

Initial version, no changes.
