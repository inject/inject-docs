## System Overview

The INJECT Exercise Platform (IXP) is architecturally structured into two distinct components:
the frontend and the backend.
This separation allows for clear delineation of roles and responsibilities,
enhancing modularity and scalability within the system.

The architecture of the IXP can be visualized as follows:

```mermaid
flowchart LR
    A(INJECT) --> B(BACKEND)
    A --> C(FRONTEND)
    C --> D(TRAINEE PANEL)
    C --> E(INSTRUCTOR PANEL)
    C --> F(ANALYST PANEL)
    C --> J(EXERCISE PANEL)
    C --> L(DEFINITION EDITOR)
    B --> G(EXERCISES)
    B --> H(EXERCISE DEFINITIONS)
    B --> I(RUNNING EXERCISE)
    B --> K(AUTHENTICATION)
```

This diagram illustrates the high-level architecture of the IXP.
It is composed out of these parts:

- Frontend: The frontend component serves as the user interface of the IXP,
  allowing users to interact with the system.
  It includes four main parts:

  - Trainee View: This view is designed for trainees participating in tabletop exercises.
    It provides functionalities tailored to their role, such as accessing exercise scenarios,
    submitting responses, and general interaction with the exercise.
  - Instructor View: This view is tailored for instructors or facilitators overseeing tabletop exercises.
    It offers capabilities for monitoring trainee progress, and providing guidance during the exercise.
  - Analytics: The analytics view provides insights and analytics related to the tabletop exercises conducted on the platform.
    It includes metrics such as participation rates, completion times, and performance trends.
  - Exercise Panel: The exercise panel enables users to view and manage exercises available on the platform,
    including creating new exercises, editing existing ones, and organizing exercise-related resources.

- Backend: The backend component serves as the backbone of the INJECT platform, handling data management,
  authentication, and communication with the frontend.
  It includes several parts:
  - Exercises: This module manages the creation, modification,
    and storage of tabletop exercises on the platform.
  - Exercise Definitions: The exercise definitions module stores templates and definitions for different types of exercises.
    These definitions include details such as injects, emails and tools.
    It also checks and ensures that definitions are correctly configured and set up.
    For a more in-depth explanation of definitions, please refer to [definitions documentation](definitions/README.md).
  - Running Exercise: This module manages the execution and runtime aspects of tabletop exercises,
    including tracking the progress of ongoing exercises and managing the flow of injects.
  - Authentication: Handles user authentication and authorization, ensuring secure access to the platform's functionalities.

## Used Technology

### Backend Technology

The backend component of the IXP is built using Python and Django, a high-level web development framework.
Key components of the backend architecture include:

- Python Backend: The backend of the IXP is implemented using Python.
- Django Framework: Django, the most popular web development framework for Python,
  is used to build the backend application.
  Django provides robust features for database management, user authentication, and more.
- GraphQL API: The backend exposes a GraphQL API for communication with the frontend.
  GraphQL allows efficient data fetching and enables the client to request only the data it needs.
- Django REST Framework: For file transfers and other functionalities not supported by GraphQL,
  a REST API is implemented using Django REST Framework.

### Frontend Technology

The frontend component of the IXP utilizes modern web technologies including:

- React Frontend: The frontend application of the IXP is developed using React, library for building user interfaces.
- TypeScript: TypeScript is used instead of JavaScript to ensure type safety and improve code quality.
- React Hooks: React hooks play a crucial role in modern React programming,
  providing essential features such as state management and performing side effects.
- Libraries and Tools: Various libraries and tools, including Yarn, Vite, Apollo Client, and Generouted,
  are utilized for package management, building, data management, and routing within the frontend application.

## Security overview

### Authentication

Authentication on the INJECT Exercise Platform is fundamental to ensuring secure access to its features and resources.
Leveraging Django's (modified) built-in session system, the platform uses HTTP header as a means to authenticate client requests.

### Authentication Process Overview

The authentication process involves the following steps:

```mermaid
sequenceDiagram
  Client->>Server: Get backend version
  Server->>Client: Respond with backend
  Client->>Client: Check version compatibility
```

1. Client → Server: Gets the backend version from the REST API.

2. Client ← Server: Responds with backend version.

3. Client: Checks the compatibility between client and backend versions and display an error in case of incompatible versions.

The next crucial step in the process is **login**:

```mermaid
sequenceDiagram
  Client->>Server: Send login request (username with password) on REST API
  Server->>Client: Send session identifier
```

3. Client → Server: Sends a login request on the REST endpoint containing the username and password.
4. Client ← Server: If the provided credentials are valid, the server generates a _new session_,
   setting the _user_ attribute of the session to the corresponding user that has successfully authenticated.
   The server then responds with a _sessionid_, containing identifier of users session.

From now on, the client requests are authenticated by the HTTP header that contains the _sessionid_ (session identifier).

For further information, refer to the official documentation on:

- [Django Sessions](https://docs.djangoproject.com/en/2.0/topics/http/sessions/)

### Basic authentication

For the purposes of more convenient development, basic authentication is also supported
for the REST API according to the [RFC 2617](https://datatracker.ietf.org/doc/html/rfc2617#section-2) standard.

<div class="navigation" markdown>
  [&larr; Installation overview](../installation/overview.md){ .md-button }
</div>
