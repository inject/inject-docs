# Platform and Documentation Version Compatibility

## In a nutshell:
- The INJECT platform comes in different versions
- Each platform version has a matching definition versions
- Check the table below to find which definition version you should use

---

| Definition Version | Platform Version |
|--------------------|------------------|
| 0.12.1 | 2.0.0 |
| 0.18.1 | 3.0.0 |


[Definition upgrade Guide](architecture/definitions/upgrade.md){ .md-button }