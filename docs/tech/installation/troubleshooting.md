## Troubleshooting

If you encounter issues during deployment, consider the following steps:

- **Executable Permissions**: Ensure that the deployment scripts have executable permissions. Run the following command to set the appropriate permissions:
    ```
    chmod +x deploy.sh 01-substitute-env.sh
    chmod +x deploy-with-https.sh generate-cert.sh  # If using HTTPS deployment
    ```

- **INJECT_DOMAIN Setting**: Verify that the INJECT_DOMAIN environment variable is set to a domain name and not a static IP address. The platform requires a domain name to function correctly. For local test deployments please use reserved TLD `.localhost` (example: `inject.localhost`)

- **Backend is not returning any valid response**: Verify that the `INJECT_DOMAIN` and `INJECT_HOST_ADDRESSES` is setup correctly. The platform requires that the addresses are setup to hostnames where the platform is expected to be accessed from.

- **Frontend's error log in Developer Console is reporting mixed-origin connection issues**: Verify that connection settings in the environment varibles are setup correctely. If using HTTP deployment, that they use `http://` and `ws://` protocol, and when using HTTPS, then `https://` and `wss://`.

- **Application is connecting to an incorrect backend instance**: Clean up your cookies and session storage to enforce rebinding of the Frontend client.

<div class="navigation" markdown>
  [&larr; Installation overview](./overview.md){ .md-button }
</div>