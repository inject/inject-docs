For version 3.0.0 of IXP, download the compose preset folder for HTTPS Deployment with your own certificates [here](https://gitlab.fi.muni.cz/inject/docker-deployment/-/package_files/230/download), unzip it, set it as your working directory, and follow the instructions below.

## HTTPS Deployment with your own certificates (advanced)

For HTTPS deployment with your own certificates, add `fullchain.pem` and `privkey.pem` into the `./certificates` directory. If the format of the keys or selection of keys does not suffice, modify the `nginx.conf.template` file and change the related configuration on line `:32-33`.

Docker compose mounts the `./certificates` folder as `/etc/nginx/certificates/` in the Nginx container.

After adding the certificates, proceed with the installation as described in the [installation guide](../overview.md).

<div class="navigation" markdown>
  [&larr; Installation overview](../overview.md){ .md-button }
  [Basic HTTPS](./base-setup.md){ .md-button }
</div>
