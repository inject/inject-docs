For version 3.0.0 of IXP, download the compose preset folder for HTTPS deployment using Let's Encrypt [here](https://gitlab.fi.muni.cz/inject/docker-deployment/-/package_files/229/download), unzip it, set it as your working directory, and follow the instructions below.

## HTTPS Deployment

For HTTPS deployment, you need to generate HTTPS certificates.
In this configuration, the certificates are generated via [Certbot](https://certbot.eff.org/) using Lets Encrypt.

This can be done via the `./cert_script.sh` script, which automatically generates the certificate in the current directory.
Invoke the script like so, substituting the placeholders for their respective values:

```bash
./cert_script.sh <email-address> <domain>
```

After the certificate has been generated,
an email will be sent to the provided address with additional information about the certificate.

The same script can be used to renew the certificates.

Example usage:

```bash
./cert_script.sh admin@inject.fi.muni.cz inject.fi.muni.cz
```

Please note that by executing this script, you're agreeing to Certbot's TOS and Privacy Policy,
and your email will be used for essential communication about expiration notices.

After adding the certificates, proceed with the installation as described in the [installation guide](../overview.md).

<div class="navigation" markdown>
  [&larr; Installation overview](../overview.md){ .md-button }
  [HTTPS Owncert](./owncert.md){ .md-button }
</div>
