## Initial user creation

INJECT Exercise Platform without prior setup does not generate any standard admin user,
that you may use to log in and manage the platform.

There are currently two ways how you may generate said Admin user,
both of which require direct access to the backend docker container.

To enter the container, run the following command:
```
docker compose exec -it backend /bin/sh
```

### Management command

**This option REQUIRES SMTP to be set up.**

In the docker container, run:
```
python manage.py initadmins <comma-separated list of emails>
```

This command will create an admin account for each email address in the list
and send the login credentials to that address.

Example:
```
python manage.py initadmins jane.doe@mail.com,john.doe@mail.com
```

### Django shell

This is the only option to create admin users without SMTP.

In the docker container, run these commands:
```
python manage.py shell
from user.models import User

User.objects.create_superuser(username="email@test.com", password="long_and_secure_password")
```

Replace `email@test.com` and `long_and_secure_password` with your desired admin credentials.

After creation of the user, you may log in into the IXP.

If you need to create normal users, then please refer to the [User Onboarding](./../security.md#user-onboarding).

<div class="navigation" markdown>
  [&larr; Installation overview](overview.md){ .md-button }
</div>
