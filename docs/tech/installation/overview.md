# Installation

The INJECT Exercise Platform (IXP) comprises two main components: the frontend and the backend.
This separation allows for modular development, easier maintenance, and scalability of the platform.
Below, we'll walk you through the unified process of installation for frontend as well as for backend.

For a deeper understanding of the overall architecture of the IXP,
you can refer to the [architecture overview](../architecture/overview.md) in the documentation.

## Prerequisites

### Hardware Requirements

Before you begin, ensure that you have the following:

- A server with at least 2 core CPU and 8 GB RAM
- A 64-bit x86 architecture
- Root or sudo access to the server
- At least 4 GB of disk space, although more might be necessary to hold files uploaded during exercises

These requirements ensure that the backend server can handle data processing efficiently while serving the frontend interface smoothly.

### Software Requirements

- Linux operating system: Debian 11 or 12 is recommended
- [Docker Engine](https://docs.docker.com/engine/install/).
- [Docker Compose](https://docs.docker.com/compose/install/linux/) with recommended version v2.27.0

Make sure to install Docker and Docker Compose before proceeding with the installation.

## Installation

### 1. Choose a Docker Compose preset and follow the instructions on setting up the TLS certificates:

- [https](./https/base-setup.md) for a TLS certificate by Let's Encrypt,
- or [https-owncert](./https/owncert.md) for setup with your own certificate.

### 2. Download the source code of the frontend and the backend:

- [frontend](https://gitlab.fi.muni.cz/inject/frontend/-/releases)
- [backend](https://gitlab.fi.muni.cz/inject/backend/-/releases)

To ensure that the downloaded versions of frontend and backend work correctly,
check that they have the same version in the name on their respective `Releases` pages.
E.g. `v2.0.0` of the frontend is fully compatible with `v2.0.0` of the backend.

### 3. Unzip the downloaded archives, move them inside the compose preset folder (`https` or `https-owncert`, depending on which one you chose for generating TLS certificates), and rename the unzipped folders to `frontend` and `backend` respectively.

Having the source code available in folders named `frontend` and `backend` is crucial for the deployment script to work.

### 4. Set up the `.env` file in the root directory (`https` or `https-owncert`).

Follow the setup guide [here](./setup.md#environment-variables). Especially make sure that:

- `INJECT_DOMAIN` is set to your desired hostname (the same one you generated the certificate for),
- and `INJECT_SECRET_KEY` is changed to a truly random string of characters of at least 50 characters.

### 5. Make sure the `01-substitute-env.sh` script is executable. You can grant the executable permission via `chmod +x 01-substitute-env.sh`.

### 6. Run `docker compose up`, or `docker compose up -d` to run the containers in the background.

If any errors occur, please refer to the [troubleshooting guides](./troubleshooting.md).

### 7. Add an admin (superuser) account by following [these instructions](./users.md).

## Conclusion

By following the installation guide, you'll be able to successfully set up and deploy the IXP.
After completing the installation,
you may download this [Intro Definition](https://gitlab.fi.muni.cz/inject/inject-docs/-/raw/citadel/files-from-repos/intro-definition.zip?ref_type=heads&inline=false)
and try to upload it to test the platform's functionality.

If you encounter any bugs,
please refer to the [Known Issues and Fixes](../../known-issues.md) page for troubleshooting steps and solutions.
If you require further assistance, don't hesitate to report them to us.
The [Report issue](../../report-issue.md) page includes instructions on how to report bugs.

<div class="navigation" markdown>
  [&larr; Home](../../index.md){ .md-button }
  [Troubleshooting](troubleshooting.md){ .md-button }
</div>
