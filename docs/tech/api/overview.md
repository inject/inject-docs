The INJECT API is designed to facilitate communication between the frontend and backend components of the INJECT Exercise Platform (IXP).
It enables clients to perform various operations, including data retrieval, manipulation, and real-time updates.

## GraphQL API

The GraphQL API provides a flexible and efficient way to interact with the IXP.
It allows clients to query specific data fields, reducing over-fetching.
The GraphQL documentation will provide detailed information about the available queries, mutations, and subscriptions, along with examples and usage guidelines.

## REST API

The REST API complements the GraphQL API by providing additional functionality and support for file transfers and other operations not supported by GraphQL.
The [REST documentation](swagger-docs.md) covers endpoints, request methods, parameters, and response formats, offering comprehensive guidance for integrating with the IXP.
