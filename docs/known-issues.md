# Known Issues and Fixes

This page outlines the known issues associated with the platform and provides guidance on how to resolve or work around them.

## Exercise cannot be started after an outage

### Issue

Cannot start an exercise even though all exercises are stopped.

### Cause

When an exercise is running, a special lockfile is created.
This lockfile is deleted automatically when the exercise is stopped.
If the backend server crashes or the server is manually stopped during a running exercise,
this file will not be properly deleted, preventing from starting this specific exercise again.
The server is set up to automatically delete all lockfiles on startup.
However, if this issue persists even after restarting the server,
it could be possible that the process failed.
Ensure that no lockfiles are present in the `data` directory with the name `exercise-<ID>.lock`.
The `data` directory is located in the base directory of the backend.
These files can be deleted while the server is running,
however only if there are no actual running exercises.

## Editor: The created definition is invalid

### Issue

Leaving the _Content_ field blank during the _Injects preparation_ phase results in the creation of invalid definitions.

### Cause

In this case, the _content_path_ in _injects.yml_ is set to a non-existing file.

### Solution

During the _Injects preparation_ phase in Editor, make sure to fill in the _Content_ field for all your injects.

## Editor: Failed to update inject (ConstraintError)

### Issue

When clicking on _Save_ or _Save & Exit_ during the _Injects preparation_ phase, the following error is shown:

![Failed to update information inject: ConstraintError: Unable to add key to index 'injectInfoId': at least one key does not satisfy the uniqueness requirements. ConstraintError: Unable to add key to index 'injectInfoId': at least one key does not satisfy the uniqueness requirements.](./images/editor-inject-preparation-error.png)

### Cause

When saving the prepared inject for the first time, the creation process triggers twice due to an error in code.
Therefore, the code is trying to create an inject with the same key (id), triggering the constraint error.

### Solution

Ignore the error.

## Conclusion

If you require further assistance, don't hesitate to report issues to us. The [Report issue](report-issue.md) page includes instructions on how to report bugs.
