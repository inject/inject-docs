# INJECT Process

## In a nutshell

- The INJECT process is a way to design tabletop security exercises with the INJECT Exercise Platform.
- It consists of five interconnected phases inspired by design thinking:
  understand, specify, prepare, execute, and reflect.
- The main advantage is that it incorporates the platform into the whole process.

---

## Overview of the phases

![](../../images/1.svg)

### 01 Understanding phase

The goal of the Understanding phase is to gather all the necessary information to prepare for the exercise.
This includes identifying the objectives, the scope, and the resources required.
During this phase, you will ensure that you have a comprehensive understanding of the context and goals of the exercise.

### 02 Specification phase

In the Specification phase, you will clearly define the exercise objectives, aligning them with the needs identified in the previous phase.
This involves breaking down these objectives into specific learning activities and creating the main and parallel inject lines that form the scenario.
The primary outcome of this phase is a well-structured specification that will guide the exercise preparation.

### 03 Preparation phase

The Preparation phase focuses on getting everything ready for the exercise.
This includes preparing the content, setting up logistics, and ensuring that all materials and resources are in place.
The aim is to be fully prepared for a smooth and successful execution of the exercise.

### 04 Execution phase

The Execution phase is about managing the exercise day.
This includes facilitating the exercise, conducting initial briefings, and performing the final hot wash.
The INJECT Exercise Platform will be used extensively to support these activities, ensuring that the exercise runs efficiently.

### 05 Reflection phase

The Reflection phase is dedicated to evaluating the exercise on multiple levels.
This includes

* assessing trainee performance and providing specific recommendations,
* reviewing the scenario for potential improvements, and
* reflecting on the overall organization and execution of the exercise.

This phase aims to identify strengths and areas for improvement, helping to enhance future exercises.

??? "Relation to INJECT Exercise Platform Components:"
    ### Editor

    The Editor is a versatile tool used during the **Specification and Preparation phases** of the INJECT Process.
    It allows exercise designers to specify exercise objectives, create scenarios, and prepare exercise content.
    With its user-friendly interface, the Editor enables efficient scenario development and content organization.

    ### Trainee View

    The Trainee View is designed for use during the **Execution phase** of the exercise.
    It provides trainees with access to the exercise scenario and injects, facilitating their participation and engagement.
    Through the Trainee View, trainees can navigate the exercise, respond to injects, and collaborate with teammates in a simulated environment.

    ### Instructor View

    The Instructor View is a comprehensive tool used by exercise instructors during the **Execution phase** of the exercise.
    It allows instructors to manage the entire exercise, from overseeing trainees interactions to facilitating discussions and providing guidance.
    With features for real-time monitoring and intervention, the Instructor View ensures smooth exercise execution and effective facilitation.

    ### Analyst View

    The Analyst View serves multiple purposes throughout the INJECT Process.
    During the **Reflection phase**, it is utilized for evaluation and data analysis, providing insights into trainees performance and exercise outcomes.
    Additionally, the Analyst View serves as a source of information during the **Understanding phase**, offering valuable data and feedback to inform exercise preparation.
    Its intuitive interface enables thorough evaluation and informed decision-making for future exercises.

## Subjects in the INJECT Process

**In organizing a tabletop exercise, various functions are crucial to ensure its success.
Depending on the context, these functions may overlap, with some individuals taking on multiple responsibilities.**

- **Client**:
  The client is the organization or part of the organization for which the exercise is prepared and executed.
  Ideally, the client understands the purpose and need for the exercise.

- **Exercise Provider**:
  This is the organization or department that oversees the entire process and appoints the exercise organizers.

- **Exercise Organizers**:
  These individuals are responsible for all aspects of preparation, including managing logistics and content preparation.
  They act as the project managers of the exercise.

- **Exercise Designers**:
  Their main role is to develop the exercise scenario.
  This includes defining the entire scenario and preparing the content for the platform.
  They are the authors of the exercise concept.

- **Exercise Instructors**:
  These individuals execute the exercise via the platform, provide briefings, and conduct the final hot wash.
  Unless specified otherwise, they also analyze the final data from the exercise.

- **Trainees**
  These are the individuals who take part in the exercise.
  Their primary objective is to learn and gain experience from the exercise.

!!! NOTE
    We distinguish between the subjects of the exercise (organizations, designers, instructors) and
    the exercise roles intended for the scenario (e.g., CSIRT, lawyer, PR specialist, national authority).

## Types of exercises

**The INJECT process is designed to guide you through organizing a successful tabletop exercise using the INJECT Exercise Platform.
There are two primary types of exercises you can consider:**

- **Strategic decision-making exercises**:
  These exercises emphasize decision-making, evaluation, and high-level planning.
  They are suited for testing strategic thinking and the ability to manage and respond to complex scenarios.

- **Process-Technical Exercises**:
  These exercises focus on simulating specific processes using designated tools.
  They are ideal for honing technical skills and ensuring that trainees are proficient in particular procedures and systems.


While these categories provide a useful framework, it is not necessary to adhere strictly to one type.
In fact, exercise designers are encouraged to blend both approaches.
By combining process-technical elements with strategic decision-making components,
you can create a more comprehensive and effective exercise tailored to your specific goals.

!!! Glossary
    We use several terms in the INJECT process that may be unfamiliar, but you can always find their definitions in our [glossary](../../glossary-of-terms.md).


## Start now

<br>
<div class="result" markdown>
  <div class="grid cards" markdown>
- [1. Understand](../understand/overview.md)
- [2. Specify](../specify/overview.md)
- [3. Prepare](../prepare/overview.md)
- [4. Execute](../execute/overview.md)
- [5. Reflect](../reflect/overview.md)
- [Glossary](../../glossary-of-terms.md)
  </div>
</div>

!!! Disclaimer
    This is the initial version of the documentation describing the INJECT process from a broad perspective.
    As we continue to develop the platform, we will expand each section with specific information and examples.

<div class="navigation" markdown>
  [Home](../../index.md){ .md-button }
</div>
