# EXECUTE: conduct and manage the exercise

## In a nutshell

- This is the fourth phase of the INJECT process dedicated to managing and conducting the exercise.
- It includes facilitating the exercise, providing initial briefings, and conducting the final hot wash.
- Uses the INJECT Exercise Platform to ensure efficient execution and data collection.

---

## Where are we in the INJECT process?

![](../../images/5.svg)

---

## Final Checks

- Conduct a walkthrough of the venue to ensure everything is in place.
- Verify all technical setups one last time.
- Confirm that all materials, equipment, and resources are ready and accessible.

## Tasks for instructors

As an exercise instructor, your role is crucial in ensuring the smooth execution of the tabletop exercise.
Here are some key tasks to keep in mind:

- **Provide Initial Exercise Brief**:
  Start by providing trainees with an initial exercise brief, outlining the context, rules, and expectations for the exercise.

- **Consider Platform Intro Tutorial**:
  Since the IXP may be new to many trainees, it makes sense to **let them to complete the introductory tutorial first**. Although the platform’s user interface is intuitive, having a structured      
     overview is more beneficial than letting trainees figure it out during the actual exercise. We have created a short introduction that will take no more than 15 minutes to help enhance their 
      experience.

    
    <div class="navigation" markdown>
        [Intro Definition](https://gitlab.fi.muni.cz/inject/inject-docs/-/raw/citadel/files-from-repos/intro-definition.zip?ref_type=heads&inline=false){ .md-button }
    </div>


- **Monitor Trainee Activities**:
  Keep a close eye on the activities of trainees throughout the exercise.
  Ensure that they are engaged and progressing appropriately.

- **Choose Appropriate Responses**:
  One of the most important tasks is to choose appropriate responses to the actions of trainees.
  For example, emails may have different response templates prepared for different situations.
  It's essential to select the most suitable response to ensure that trainees do not get stuck.

- **Utilize Role Play in Story**:
  When nudging trainees, consider using role play within the story context.
  Clever hints that suit the scenario context are often more effective than using your authority as an instructor.

- **Be Prepared to Improvise**:
  While much of your knowledge is embodied in response templates,
  be prepared to improvise if the trainees' reactions are unexpected or deviate from the planned scenario.

- **Provide Hot Wash**:
  Finally, conduct a hot wash session after the exercise to
  debrief trainees, discuss key learnings, and provide feedback on their performance.
  Also ask for their feedback on the scenario.

- **Interview Instructors**:
  After the hot wash session, conduct a group interview with the exercise instructors to gather their immediate feedback.

!!! Disclaimer
    In this part of the documentation, we will provide a guide for the Instructor View.
    Nevertheless, it's important to note that
    the complete user guide for the Instructor View is currently undergoing significant iteration.
    Therefore, this section is not included at this time.

<div class="navigation" markdown>
  [&larr; 3 Preparation phase](../prepare/overview.md){ .md-button }
  [5 Reflection phase &rarr;](../reflect/overview.md){ .md-button }
</div>

<div class="navigation" markdown>
  [INJECT Process Overview](../intro/overview.md){ .md-button }
</div>
