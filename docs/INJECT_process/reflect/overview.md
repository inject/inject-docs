# REFLECT: evaluate and improve the exercise

## In a nutshell:

- This is the fifth phase of the INJECT process, aimed at evaluating the exercise on multiple levels.
- Focuses on assessing trainee performance, reviewing the scenario, and reflecting on the overall organization and execution.
- Provides specific recommendations for improvements and identifies strengths to enhance future exercises.

---

## Where are we in the INJECT process?

![](../../images/6.svg)

---

## Log analysis
- If you have an external tool, you can utilize logs - see the [log specification](../../tech/log-format.md) for details.

## Trainee Performance Evaluation

- Reflect on how trainees performed based on the established learning objectives and activities.
- Analyze both individual and group performance throughout the exercise.
- Recognize areas where trainees excelled and demonstrated competency.
- Highlight aspects where trainees faced challenges or showed gaps in understanding.

### Provide recommendations for improvement

- Offer clear recommendations for enhancing trainees performance in relation to the exercise objectives.
- Suggest practical insights for addressing identified weaknesses and leveraging strengths.

!!! Disclaimer
    In this part of the documentation, we will offer a guide for the Analyst View.
    However, please be aware that the comprehensive guide to its usage is still undergoing substantial revisions.
    Consequently, this section is not currently featured in the documentation.

## Scenario Review

- Recognize any shortcomings or areas for improvement within the scenario.
- Consider feedback from trainees and facilitators on areas that could be strengthened.
- Propose specific changes or additions to the scenario to enhance realism, engagement, and learning outcomes.

## Organization and execution reflection

- Reflect on the effectiveness of organizational processes in planning and executing the exercise.
- Document best practices and strategies for improving future exercise planning and implementation.

## Final Remarks

Never overlook the importance of the reflection phase.
It serves as an input for your next understanding phase when planning another exercise.
Remember, the knowledge gained from one reflection can directly impact your next exercises.

<div class="navigation" markdown>
  [&larr; 4 Execution process](../execute/overview.md){ .md-button }
  [INJECT Proces overview](../intro/overview.md){ .md-button }
</div>
