# PREPARE: get everything ready for the exercise

## In a nutshell:

- This is the third phase of the INJECT process, aimed at preparing all necessary content and logistics.
- Ensures that all materials, resources, and logistical arrangements are in place for a smooth execution.
- Prepares everything needed for a successful and efficient exercise day.

---

## Where are we in the INJECT process?

![](../../images/4.svg)

## Exercise Content creation

This part of preparation involves filling the structure of the defined scenario with concrete text, pictures, other media, or logical rules.
In this phase, you will prepare the content for injects such as emails, files, questionnaires, and tools.
You will also set up details for the learning activities of the trainees.

The preferred method for content creation in the future is using the Editor, an INJECT Exercise Platform component that is currently in the prototype stage.
However, there is an alternative option available: you can prepare content directly by creating and editing YAML files.

??? "How to prepare content in YAML?"

    ## General Remarks

    - Start with Examples: First of all, check the example of definition in GitLab.
    It is a great starting point.

        <div class="navigation" markdown>
          [INJECT Definition Example](https://gitlab.fi.muni.cz/inject/inject-docs/-/raw/citadel/files-from-repos/showcase-definition.zip?ref_type=heads&inline=false){ .md-button }
        </div>

    - Then look at the definition details section: 
        <div class="navigation" markdown>
         [Definition details](../../tech/architecture/definitions/README.md){ .md-button }
        </div>

    - In case you want to upgrade the current definition to a newer version:
        <div class="navigation" markdown>
        [Definition Upgrade](../../tech/architecture/definitions/upgrade.md){ .md-button }
        </div>

    ### Additional Notes
    - Iterate and Test: Iterate and test a lot to ensure the content is accurate and functional.
    - Most of the typos and errors in the YAML format are caught by the validator in the platform.
    - Markdown for Complex Texts: If there are any longer or more complex texts or responses (especially those with special characters), it is recommended to use markdown files in the content folder.
    - If you use overlays, do not set them to start at minute 0 of the exercise - set them to minute 1 instead. Otherwise, the overlay would not appear for trainees.

    ## Crucial Concepts

    ### Injects and Inject Categories and Inject Selections

    A core idea the exercise definition is built on is that an inject category (IC) groups injects, for which it only makes sense to send one of them to the team.
    These can, for example, be variations of the same inject, where one is sent if condition A is met and B is sent, then the condition is not met.
    Inject categories can either be evaluated and sent to the team automatically or manually by an instructor.
    In case the injects are evaluated manually by an instructor, they transform from an IC into an inject selection (IS).
    With the IS in hand, an instructor can select any of the injects in it and forward them to the trainees.
    They may also change the inject in any way before actually sending it to the team, so in case some information is not known at the time of creating the definition and cannot be specified as the inject condition, you can add a message for the instructor into the inject itself, something like "...send email to [INSTRUCTOR EDIT HERE] and...".
    Just make sure to tell this to the instructors, as if they do not edit this and select the inject, it will be sent as is, breaking the immersion for the trainees.

    ### Conditional Injects

    As described in the exercise definition repository, adding multiple injects into one inject category (IC) produces conditional injects, which may be sent based on a specific situation happening in the scenario.
    This may be used to create branching in the scenario, or just to make the exercise feel more responsive and realistic, where actions of trainees have notable consequences and impact on how the scenario plays out.

    ### Milestones

    Milestones are an element that connects the entire exercise definition together.
    Each team has its own set of milestones to reach independently of other teams.
    They define interactions between injects, tools, and emails.
    Whenever there is an action that is supposed to produce a response from the application, whether automated or semi-automated, a milestone must be linked to this action and the corresponding response.
    If the usage of tool is supposed to have an impact on an exercise, a milestone has to be linked to this tool usage and to the impact.
    They are by default invisible to trainees, but instructors see them and can use them to evaluate the state of each team.
    A set of all milestones the team has reached clearly identifies the point of the exercise this team is in.

    ### Email Tool

    An email tool is used for simulating conversations between trainees and other entities in the exercise.
    As an exercise may be rather complicated, the instructor may not know there exact details about each of the correspondents they are supposed to act as.
    For this reason, you may use the description field of each address in email.yml to clearly explain to the instructor what the purpose of each correspondent is.
    If the corespondent is expected to react a certain way to certain situations, use templates.
    These give instructor pre-scripted email choices


??? "Editor"
    The INJECT Exercise Platform now includes a new tool - the Editor - that makes exercise creation more accessible and guided. While it's still in development, it offers a user-friendly alternative to manual YAML editing.
    <div class="navigation" markdown>
    [Learn more](../prepare/editor.md){ .md-button }
    </div>


## Exercise logistics

??? "General Checklist"

    ### Venue Arrangements

    - Book the exercise location.
    - Ensure the venue has necessary facilities (e.g., Wi-Fi, power outlets, seating arrangements).
    - Confirm availability of AV equipment (projectors, screens, microphones).

    ### Instructors

    - Instructors are key to the implementation of the exercise.
    - In the basic model we need one instructor per team.
    - Specialists? Not needed - their knowledge is embodied in the response templates.
    - If the instructors are not experienced you should have a dry run with them.

    ### Equipment and Materials

    - Prepare and test all necessary equipment (computers, tablets, printers).
    - Ensure backup equipment is available.
    - Print all required physical materials.

    ### Communication Plan

    - Confirm contact information for all trainees, instructors, and support staff.
    - Prepare and schedule pre-exercise communications (reminders, agendas).

    ### Trainees Readiness

    - Distribute pre-exercise materials and instructions to trainees.
    - Schedule a pre-exercise briefing session if necessary.
    - Ensure trainees have completed any required pre-exercise training or assessments.

    ### Logistics Coordination

    - Confirm catering arrangements, if applicable.
    - Arrange transportation and accommodation for trainees and staff, if necessary.
    - Ensure all logistical support staff are briefed and understand their roles.

??? "Platform Setup"
    - Ensure that exercise definition is ready.
    - Test it in the platform.
    - We suggest to organize a dry run at the end of the Prepation phase.

    ### How to Launch an Exercise on the INJECT Exercise Platform

    1. **Navigate to the Exercise Panel**  
        - Access the exercise panel.
        ![](../../images/exercise_panel.png)
        - The exercise panel has two main sections:
          - **Definitions**: This section handles definitions. You can upload, remove, and manage instructor access to them.
          - **Exercises**: This section handles exercises. You can create, pause, remove exercises, add trainees, and download logs.

    2. **Upload an Exercise Definition**
    
        - In the `Definitions` section, click the `+ Add` button to upload a definition.
        - You may use the showcase definition provided here:

          <div class="navigation" markdown>
          [INJECT Definition Example](https://gitlab.fi.muni.cz/inject/inject-docs/-/raw/citadel/files-from-repos/showcase-definition.zip?ref_type=heads&inline=false){ .md-button }
          </div>

    3. **Create an Exercise**
        - In the `Exercises` section, click the `+ Add` button.
        - A window will pop up asking you to select a definition and configure exercise options such as `Number of teams` and `Exercise name`.
        - After configuring these options, click `Submit` to finish creating the exercise.

    4. **Add Trainees**
        - After creating the exercise, click on the `Trainees` tab under the exercise you have created.
        - Here, you will see all the accounts that were automatically created, and you can assign each one a trainee.

    5. **Start the Exercise**
        - Once all configurations and settings are completed, click the `Start` button under the exercise you created.
        - This action will successfully launch the exercise on the INJECT Exercise Platform.

    By following these steps, you will be able to successfully set up and run an exercise on the INJECT Exercise Platform.


??? "Onboarding"
    Since the IXP may be new to many trainees, it makes sense to **let them to complete the introductory tutorial first**. Although the platform’s user interface is intuitive, having a structured      
     overview is more beneficial than letting trainees figure it out during the actual exercise. **We have created a short introduction that will take no more than 15 minutes to help enhance their 
      experience.**
    
    <div class="navigation" markdown>
        [Intro Definition](https://gitlab.fi.muni.cz/inject/inject-docs/-/raw/citadel/files-from-repos/intro-definition.zip?ref_type=heads&inline=false){ .md-button }
    </div>



<div class="navigation" markdown>
  [&larr; 2 Specification phase](../specify/overview.md){ .md-button }
  [4 Execution phase &rarr;](../execute/overview.md){ .md-button }
</div>

<div class="navigation" markdown>
  [INJECT Process Overview](../intro/overview.md){ .md-button }
</div>
