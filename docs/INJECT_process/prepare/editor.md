# Editor: A Tool for Exercise Creation

## In a nutshell:
- The Editor is a user-friendly interface for designing INJECT exercises.
- It makes exercise creation accessible without requiring YAML expertise, because it guides designers through a structured creation process.
- Currently supports [definition version 0.13.0](https://inject.pages.fi.muni.cz/inject-docs/tech/architecture/definitions/upgrade/).

---

## Where are we in the INJECT process?

![](../../images/4.svg)


## What is the Editor?

The Editor is a new component of the INJECT Exercise Platform that innovates how exercises are created. 
While exercises have traditionally been defined manually in YAML format (requiring technical expertise), the Editor provides an intuitive interface that guides you through the exercise creation process.

### Key Benefits
- **User-Friendly Interface**: No YAML knowledge required
- **Guided Process**: Step-by-step exercise creation
- **Structured Approach**: Systematic organization of exercise components
- **Automated Generation**: Produces valid YAML exercise definitions automatically


### Should You Use the Editor?
The Editor is recommended for:
- New exercise designers
- Those unfamiliar with YAML
- Exercises that don't require advanced features

Consider manual YAML editing if you need:
- Latest platform features
- Complex milestone configurations
- Role-based exercise design
- Multiple channel configurations

## How to start?
* The Editor is easily accessible through the INJECT Gateway - see the picture below.
* Once you start using it, it will guide you through a structured process.

![Editor interface in INJECT Gateway](../../images/editor.png)

## Current Limitations

Before using the Editor, it's important to understand its current limitations to make an informed decision about whether it's the right tool for your exercise.

### Version Compatibility
The Editor currently works with an older version of the [exercise definition](https://inject.pages.fi.muni.cz/inject-docs/tech/architecture/definitions/upgrade/) format (v0.13.0) than the one used by the Citadel platform (v0.14.0+). This means:

**Supported features:**

   - Basic exercise structures
   - Most common features

**Unsupported features:**

- Newer platform capabilities
- Multiple channels of the same type

### Milestone Management

- While the Editor handles common milestone scenarios well, some advanced features are limited:

**Supported Milestone Triggers:**

- Learning activities
- Standard injects
- Tool responses
- Email templates

**Currently Unsupported:**

- File download triggers
- Arbitrary email conversation initiations
- Complex milestone activation/deactivation patterns
- Specific questionnaire answer triggers

### Other Considerations

Some additional limitations to keep in mind:

**Single Exercise Definition Focus**

   - You can work with only one exercise definition at a time
   - Saving and loading different exercise definitions requires manual file management

**Role Management**

   - No direct support for role-based exercises
   - Roles need to be added manually after export

**Compatibility with Existing Exercises**

   - Some existing exercises may need restructuring to work with the Editor
   - Complex conditions might require simplification

**Role Management**

   - No direct support for role-based exercises
   - Roles need to be added manually after export

**Compatibility with Existing Exercises**

   - Some existing exercises may need restructuring to work with the Editor
   - Complex conditions might require simplification
   

!!! tip "Working Around Limitations"
    Most limitations can be addressed by making manual adjustments to the YAML file after using the Editor. If you need these features, consider using the Editor for initial setup and then fine-tuning the exercise definition manually. Check the: [Exercise definition description](../../tech/architecture/definitions/README.md).



<div class="navigation" markdown>
  [INJECT Process Overview](../intro/overview.md){ .md-button }
</div>
