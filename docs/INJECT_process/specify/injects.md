# INJECTS

## In a nutshell
- Inject is the information that we send to the trainees of the exercise and to which they have to react. 
-This is a very simple inject: "Suddenly no computers are working in your organization. How will you react?" 
- By using a digital platform, we have many more options that can be combined in the exercises. 


---

## Where are we in the INJECT process?

![](../../images/3.svg)

---

## Necessary concepts: 


??? "Channels"
      Channels are the locations within the platform where injects appear. Each channel has a name, which can be set to any string, and a type, which defines the form of the injects it contains. The basic channel types are:

      - **Exercise    information** – general channel type for communication about exercise
      - **Emails** – classical email communication
      - **Tools** - tools outputs
      - **Questions** - a channel type where various interactive injects (e.g. questionnaires) are displayed. 

      Media channels can be simulated by creating additional channels of type _Exercise information_. The implementation of a designated media channel type is being considered:
      
      - **Website** - for simulating websites of different organizations 
      - **Intranet**
      - **Social media** - May carry the name of a specific media outlet.
      - **Media** - injects in the form of articles, audio or video. May carry the name of a specific media outlet. 


??? "Overlay"
      This is an interface effect that directly affects the dynamics of the exercise. While normally, for example, the questionnaire will be displayed in the preset channel, **if this inject is also set as an overlay, it will be displayed first above everything else**. 
      <br>
      
      - Example, the overaly questionnaire will cause everything else to go dark and it will appear in the middle of the screen. This makes the inject disrupt the trainees' existing activity and draws their attention. Some strategy exercises may consist entirely of injects presented through an overlay. 


??? "A caption: "Not implemented""
      The injects with this caption have not yet been implemented in the platform. They are likely to be added in the future, though they may be modified based on team discussions.



## INJECT Options
Let's now take a detailed look at the different inject options. This is not a final list; you can create any inject options you want from the basic elements in the platform. However, we describe these options because they have proven valuable in most exercises. Think of inject options as proven building blocks that you can use to construct your exercises. We describe their typical use in an exercise and the related learning activities. [Learning activities](../specify/learning_activities.md). 

??? "1. Inject option: Email"
    ### Inject description
      This is a classic email conversation. It can also contain email attachments. The sender address is also an important part of this option of inject and is fully configurable. Emails in the form of an inject can be sent either automatically or as an instructor activity. 
    ### Channel type
      Only a specific channel type for emails. It functions as a simple email client. 
    ### Overlay
      Emails are usually not used in conjunction with overlays.
    ### Typical use in an exercise 
      Emails can be used in any type of exercise. They are essential in process-technical exercises. 
      Strategic decision-making exercises can be fully conducted via email, but require a scenario designed to work with specific individuals (concrete names and positions) within the organization.
    ### Possible mistakes 
      - An important prerequisite for the use of emails is that the exercise trainees know their "game identity". In other words, they need to be clear why they are receiving the email. If you, as an instructor, do not communicate this well, there may be misunderstandings that result in not being able to use the pre-prepared template responses. 
      - You must identify all email addresses prior to the exercise. Omissions cannot simply be resolved during the exercise, any address must be entered into the platform in advance. Therefore, think carefully about their structure and meaning. For example, ensure that all emails within the organisation have the same spelling. Also consider creating email addresses to give to trainees even if you don't intend to use them - it completes the scenario better and adds more decision-making for trainees. 
      - Another possible error is a misunderstanding of the context for which this tool is suited. Could you imagine dealing with a truly escalated crisis situation through emails? Or would it be more likely that the team would be more likely to meet physically or at least make a phone call? 
      - Consider whether it is necessary to specifically instruct trainees at the beginning of the exercise that the emails are legitimate and that they do not need to look for phishing attempts. In one of our first exercises, we had trainees simply not open the initial email because they thought it was phishing - in short, they expected it in a cybersecurity exercise.
    ### Related manifestations  in the platform 
      - Establishing email communication - trainees sent an email. LA is filled just by sending an email. Only 1x can be used for each email address. 
      - Email reply from the instructor - see above. 
    ### Examples 
      - Internal communication in the organisation.
      - Inter-organisational communication. 
      - Communication with oustsorced co-workers. 
      - Communication with journalists. 
      - Etc.


??? "2.	Inject option: Execise information "
    ### Inject description 
      It is used to communicate basic information about the exercise - e.g., introductory inject, contextual information, exercise time shift information, closing information. 
    ### Channel type
      Exercise information. 
    ### Overlay
      The Exercise information inject can be displayed as an overlay, this is especially useful for important notifications or hints. 
    ### Typical use in exercise 
      This inject option is defacto a form of instruction, so it can be used in any exercise. 
    ### Possible mistakes 
      - Mixing "ingame" information and information outside the exercise. Our recommendation is that all information should be communicated in a manner as appropriate to the scenario as possible. In other words, we recommend omitting information that shifts the context out of the scenario (e.g., noting that there is a catering ready, etc.). 
    ### Related manifestations  in the platform 
      - Depending on the information presented, any LA may follow up. However, the Click on the Confirmation Button has a specific use - e.g. in situations where we want trainees to consciously end the phase of reading more extensive input information and move on. 
    ### Examples 
      - Information about the exercise identity of the trainees. 
      - Basic rules of the exercise. 
      - Information that the next injects take place at a different time than the previous one.
      - Context information that cannot be naturally communicated within other injects. 
      - Summary of the exercise. 


??? "3.	Inject option: Document"
    ### Inject description 
      This inject means sending a pdf document to the trainees to read, analyze or make a decision based on it. 
    ### Channel type
      The document is most often used in the Exercise information channel type , but can also be presented in other, more specialized channel types such as an intranet or website. Last but not least, it can also be sent by an email or using tools (more in the specific section -> [Tools](../specify/tools.md)). 
    ### Overlay
      It can be used.  
    ### Typical use in the exercise
      Sending reports, analyses, briefings, internal regulations, etc. At the same time, the document can serve as a certain assignment, a framework, which is then followed by other injects - for example, questionnaires or decision point. 
    ### Possible mistakes 
      - Sending a long document. If you need trainees to read a large amount of text, do everything you can to get them to do so before the exercise - e.g. sending handouts in advance. 
    ### Related manifestations  in the platform 
      - It depends on which channel type you are presenting the document in and what the content is. For example, the document can be initiated by LA, which will be linked to an email response from the instructor, etc. 
      - It can be very useful to link to a click on the confirmation button. Imagine a document display with a button below it that says "Done", "Analysis complete", "Understood", "Read", etc. 
    ### Examples
      - Incident response plan
      - Analysis of the media situation
      - Threat analysis
      - Expert group report
      - Warning from the national authority
      - Opinion of the Authority 
      - Etc. 



??? "4.	Inject option: Questionnaire/scale"
    ### Inject description
      Standard questionnaire - single choice or multiple choice. It can also act as a scale. 
    ### Channel type
      They are displayed in a special channel type, which we refer to here as Questions.  
    ### Overlay
      The use is very appropriate. 
    ### Typical use in exercises
      This inject option is very suitable for strategic decision-making exercises for presenting scenario-related questions. For process-technical exercises it can also be used, for example towards the end of the exercise, to reflect on the steps taken. 
    ### Possible mistakes 
      - Avoid making the whole exercise just a questionnaire. Such exercises do exist, but it is not engaging for the trainees and it does not fully exploit the potential of the platform. 
      - Avoid making the exercise look like a knowledge test. Questionnaires can often be about expressing opinions (e.g. scales) rather than factual accuracy. 
    ### Related manifestations in the platform
      - Submiting of questionaire is manifestation itself. 
    ### Examples: 
      - Assessment of the gravity of the situation
      – Reflection of actions
      - Probability assessment
      - Answer a factual question about the scenario (legal, organisational and technological aspects, competences, ...).
      - Etc.

??? "5.	Inject option: Decision point" 

    ### Inject description
      This is a similar inject to the questionnaire. However, the difference is that some options can be linked to another, automatic response in the platform. The number of options for a decision is 2-5. 
      - Simple example: decide the situation to communicate YES/NO to the public. If you choose "NO", the platform will respond by receiving an email a few minutes later from a curious journalist who has heard about the situation. 
    ### Channel type
      These injects are displayed in a special channel type, which we refer to here as Questions. 
    ### Overlay
      The use is very appropriate. 
    ### Typical use in exercises
      This inject option is very suitable for strategic decision-making exercises for situations where it is necessary to choose one of the options. It is particularly useful when you want to emphasize the importance of a decision. Using this inject option will draw more attention to the decision and is likely to lead to discussion. 
    ### Possible mistakes
      Avoid creating too many alternative paths - such an exercise will be much more difficult to prepare and most of the content will not be seen by trainees anyway. Simplification is desirable.
    ### Related manifestations in the platform 
      - Submiting of decision point is manifestation itself. 
    ### Examples: 
      - Ransomware ransom
      - Contacting the authority/stakeholder
      - Escalation
      - Declaration of a state of emergency. 
      - Etc.



??? "6.	Inject option: Free form"
   
    ### Inject description 
      Inject with open response, can contain input in the form of image, video or text. Trainees respond in the form of free text.    
    ### Channel type
      These injects are displayed in a special channel type, which we refer to here as Questions.
    ### Overlay
      The use is very appropriate.
    ### Typical use in exercises
      This inject is very suitable for strategic decision-making exercises.  
    ### Possible mistakes
      - Too long or complex assignments.
      - Not using the possibility of conditioned responses to the free forms. It can enhance the exercise trainees. 
    ### Related manifestations in the platform
      - Firstly, there is the actual submiting of the free form inject, which is sufficient if we don't need to evaluate the content during the exercise. 
      - Secondly, it is the instructor's response - i.e., the instructor reads the content and evaluates the fulfillment of a predefined condition - accordingly, he chooses a response that can trigger further automated steps. 
    ### Examples: 
      - A short description of an incident in the text and a request for trainees to briefly describe the first steps they will take in their organisation in response. 
      - Similarly, they can present arguments, summarise their position, assess the situation, etc. The assignment can take the form of text, image or video. 



??? "7.	Inject option: Media"
    ## Not yet implemented
    ### Description of the inject
      Media injects are currently a combination of media outputs and specifically named channel types that is an abstraction of a real-world media (the platform does not attempt to mimic the look of social networks or websites). 
      - There may be more than one such channel type in each exercise. Let's take a few examples: websites of different organizations, social networks, mass media or intranets. Injects can be in the form of plain text, or graphic materials (e.g. a Facebook post screenshots) or a prepared video can be inserted into the platform. 
    ### Channel type
      In general, we refer to this channel type as "media." It's likely that multiple media channel  with different names can be used during the exercise. For example, you could have one media channel  called "FB" to display screenshots of FB posts, and another channel for the national cybersecurity authority's website to display warnings.
    ### Overlay
      It may be appropriate in some cases - e.g. breaking news, warnings, etc. It always depends on the exercise scenario. 
    ### Typical use in exercise: 
      - Website or intranet: e.g. www.narodniautorita.cz and posts here can be titled as blog, warning, etc. In the same logic, the feeds can be, for example, the website of a practitioners' organisation or other relevant body. Pre-prepared graphics (i.e. screenshots from the web, etc.) can also be used.
      - Mass media: works similarly to the previous one. It is very useful to use pre-prepared graphic materials or videos. The media can report on a current crisis that affects the practitioners directly or just changes the context of the exercise. 
      - Social media –specific posts can influence the perception of the situation in the exercise from the point of view of ordinary people or our target groups. Again, this can relate directly to the situation in the scenario (response to a service outage) or a more general trend that will influence future decision making. 

    ### Possible mistakes
      - The platform does not currently style specific media channels - it is therefore advisable to use pre-prepared graphics or videos within them. 
      - Inconsistency with the "media behaviour" of the target group: choose channels that are actually relevant for the exercise. Do not try to use media channels just because you have the opportunity.  
    ### Related manifestations  in the platform 
      - It very much depends on the context of the scenario. The related manifestations  in the platform can be very explicit. For example, we expect trainees to immediately contact their PR department in response to the report they have seen, or we may ask them some form of question - an interactive inject - depending on the events presented in the media. Finally, we can imagine that media injects only help to complete the context of the exercise and are not closely linked to any specific activity. 

    ### Examples: 
      - Negative reactions on Facebook in response to our service outage. 
      - TV report on the terrorist attack in our city. 
      - A hateful blog post on a organization's website that appeared here because of stolen login credentials. 
      - Warning from the IT department on the organisation's intranet. 

??? "8.	Inject option: Off-platform activity"
    
    ### Inject description
      Sometimes it can really make sense to include an off-platform inject. This increases our possibilities for creating interesting scenarios. Technically, this is an instruction in the platform that is combined with a confirmation button. Example: Instruction 'Discuss now three action steps that you could implement in your organization later this month. Once you have that, click on the button." And below that instruction would be a confirmation button "Done".
    ### Channel type
      The confirmation button will display in Exercise information channel type.  

    ### Overlay
      It is very useful for this inject option. 

    ### Typical use in exercise: 
         - Invitation to trainees to discuss something.
         - Invite a representative of the team to attend a physical interview with the journalist. 
         - Call for a representative to go to the classified room and see documents that other members do not have access to. 
    ### Possible mistakes 
         - By having the activity take place outside the platform, think about its evaluation. It may be followed up by other LAs - e.g. writing a summary in an email to a supervisor, etc. 
         Or you can also decide to evaluate it outside the platform - e.g. a journalist will conduct an evaluation of the interview, according to the criteria given. 
    ### Related manifestations  in the platform
         - Click on the confirmation button. However, it should be added that off-platform activities can also be stimulated by other injects - for example, an email instruction arrives for an off-platform task, after which a response is required. Thus, it mainly depends on the creativity of the designer. 
    ### Examples: 
         - Crisis interview. 
         - Press conference. 
         - Convening a crisis meeting that takes place in person and where, for example, trainees must present the situation to management. 
         - Discussion on a predefined topic. 
         - Telephone interview.
         - Obtaining information from a classified document. 


??? "9.	Inject option: Hint"
    ### Inject description
      It's a form of conditioned inject that activates if trainees miss an action, take the wrong action, or become stuck.
      Example - trainees did not report the incident to management, but should have. The hint can be automated or sent by the instructor on an ad hoc basis. 
      - **Automated hints:** are set in advance, in response to something happening or not happening by a certain time. These hints are set based on exercise designer intuition about what might be causing the problem or data gathered from earlier runs of the exercise. 
      - **Ad hoc hints:** are created and sent by the instructor in response to unexpected developments during the exercise. 
    ### Channel type
      Hints are displayed in the exercise information channel type. 
    ### Overlay
      It is very suitable to use it. 
    ### Typical use in an exercise
      - We want to alert trainees to a misstep, an omission of an action, or help them move on. 
      - You can use a hint to notify about incoming emails—especially useful in strategic decision-making exercises or in scenarios where trainees do not work with emails regularly.
      - If you were creating some form of tutorial, you could also use them for positive feedback. 
    ### Possible mistakes 
      - Overuse of hints - they should be used very sensitively and only when absolutely necessary - i.e. when it is not possible to nudge the trainees with another form of inject - for example with an email from an exercise entity (this form of ingame hint is usually much better).  
      - Overuse of ad-hoc hints - if possible, rely instead on pre-prepared hints that come for selected situations. 
      - We do not recommend using hints to give positive feedback during exercise because uncertainty is an important part of the exercise. 
    ### Related manifestations  in the platform: 
      - The purpose of the hint is to alert trainees that they should engage in a learning activity. 
    ### Examples: 
      - Trainees forget to contact their CISO, the whole scenario freezes because of this. Hint suggests to do it.
      - The trainees convened the crisis staff too early - hint suggests to proceed with further communication after the requirements given in the respective process have been fulfilled. 
      - In the tutorial we've designed for our students, we aim to confirm the correct use of the tool. Specifically, when the student clicks on the required action using the tool, an overlay hint appears, confirming that they have successfully completed the step.


## Conditional injects 
It should be emphasized that this is not a specific inject option, but a feature of any inject options. 
It means that a conditional inject comes to the trainees in response to the fact that something has either happened (we have decided on some option) or something has not happened by a certain time and it is e.g. appropriate to send a hint.
This is further explored in Section called [Advanced approaches](../specify/advanced_approaches.md). 


<div class="navigation" markdown>
 [&larr; Learning activities](../specify/learning_activities.md){ .md-button }
 [Tools &rarr;](../specify/tools.md){ .md-button }
</div>

<div class="navigation" markdown>
 [Specification Phase Overview](../specify/overview.md){ .md-button }
</div>









