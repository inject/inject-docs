# SPECIFY: objectives, activities, injects, exercise

## In a nutshell

- This is the second phase of the INJECT process, focused on exercise specification. We assume phase assumes that the exercise designer is already familiar with the scope as defined by the Understand phase.
- This is the most important of all phases. It can be compared by analogy to the preparation of a construction plan.
- The goal of the phase is to specify an exercise that meets the needs of the client and can be implemented in the IXP environment. 


## Quick navigation

<br>
<div class="result" markdown>
  <div class="grid cards" markdown>
- [Learning objectives](../specify/learning_objectives.md)
- [Learning activities](../specify/learning_activities.md)
- [Injects](../specify/injects.md)
- [Tools](../specify/tools.md)
- [Exercise specification](../specify/exercise_specification.md)
- [Advanced approaches](../specify/advanced_approaches.md)
  </div>
</div>
---

## Where are we in the INJECT process?

![](../../images/3.svg)

---

## The description of this phase
It is divided into five sections, which describe all the essentials. It is recommended to go through them one by one, as they build on each other and form a kind of simple introductory crash course to help you use the possibilities of IXP. 

1.	First, we will discuss **[Learning objectives](../specify/learning_objectives.md)**, which will define what exactly the trainees are supposed to learn or practice. 

2.	Next, we will focus on **[Learning activities](../specify/learning_activities.md)**. Each learning objective is about specific actions that we expect from the trainees of the exercise.

3.	The third section focuses on **[Injects](../specify/injects.md)**, which are in turn inputs designed to stimulate learning activities. Simply, you can think of them as tasks. 

4.	The fourth section describes one of the innovations that the IXP brings, and that is **[Tools](../specify/tools.md)**. They allow TTX to be enriched by the abstract use of organizational or technical measures.

5.	In Section 5, we will look at **[Two basic ways how to specify exercises](../specify/exercise_specification.md)**.

6.	The sixth section focuses on **[Advanced approaches](../specify/advanced_approaches.md)**. Specifically,the possibility of using conditioned responses, and the connection of all elements. 

7.	The seventh section /*has not been addressed yet*/ should cover the **different roles in the exercise**. 


!!! Important 
    - **Please note, this is not a step-by-step guide!** Why? The three basic elements of the exercise, learning objectives, learning activities and injects, are closely related. They need to be understood first and then worked with.
      ![](../../images/8.svg)
    - There is no point in defining learning activities until you have at least a framework of what injects you will be able to prepare.  Nor does it make sense to design the injects first and then to think of objectives and learning activities for them. 
    - In short, there is a dynamic between all three parts that requires initial thought. But that investment will pay off handsomely in the quality of the exercise and the experience you prepare for your trainees. 
  
!!! Glossary
    We use several terms in the INJECT process that may be unfamiliar, but you can always find their definitions in our [glossary](../../glossary-of-terms.md).




<div class="navigation" markdown>
  [&larr; 1 Understanding phase](../understand/overview.md){ .md-button }
  [3 Preparation phase &rarr;](../prepare/overview.md){ .md-button }
</div>



<div class="navigation" markdown>
  [INJECT Process Overview](../intro/overview.md){ .md-button }
</div>

