# Tools

## In a nutshell
- In this section we will focus on the tools that are one of the innovations that IXP brings to tabletop exercises. 
- The tools allow to enrich the exercise by using organizational or technical measures.
- You don't have to use the tools in the exercise, but it's good to know about this option. 

---

## Where are we in the INJECT process?

![](../../images/3.svg)

---

## Basic types of exercises 
Tools are abstractions of actual technical tools or organizational processes. **This means that they perform the same function, but they do not look the same and simplify the situation considerably.** They work only on the key-value principle and are listed in a special channel called "Tools".


### Some examples of usage: 
-	When you enter an IP address in the tool box, the address is blocked in the context of the scenario. The tool will return information about the blocking and other actions can follow (e.g. email of an angry user). 
-	When the "Report an incident to the Authority" tool is activated, information about a successful report is displayed, and in the reality of the scenario, trainees can then receive an email from the Authority.  
-	A specific tool can be a contact list, which returns the relevant contacts for the exercise after listing the argument. 
-	The tool can also return a pdf document - for example, you can create a tool to request a report that will then come directly into the Tools channel.

### What Can Be a Tool:
-	Organization-wide user alerts.
-	Report the incident to the authorities.
-	Issue of the TS.
-	Convene the crisis meeting. 
-	IP address blocking.
-	Listing network traffic.
-	DNS lookup.
-	Contact database.
-	Antivirus.
-	Backup.
-	Managing updates.
-   Etc.


## Good to know

- Tools outputs in the "Tools" channel. 
- The use of the tool is an LA to be related to a specific LO. 
- The output from the tool can initiate another LA -> for example, we can get the necessary contact details and send an email. This means that I don't have to send this information to subscribers via specif inject anymore (but I can). 
- The output from the tool doesn't have to be just text - it can also be an image from the actual tool, for example. 
- Especially technically oriented users can look for real solutions behind the tools. Make sure they understand the nature of the tools in the platform. 
- Don't give trainees tools they don't even normally have. If your trainees are part of the communications team, they probably won't be responsible for reporting the incident to the authorities or blocking the IP address. 
- Don't oversimplify the wrong tools in relation to tool - if the trainees are in the role of CEO or board, the tool to issue a press release makes sense. If they're members of the communications team, this tool is more likely to put them in a bind - they'll want to write the release themselves in that case. 
- For tools that are organisational measures, it will often be the case that a similar thing could also be done by email. For example, organization-wide user alerts against phishing. If we make a measure into a tool, we give it a special importance and the trainees become more aware of it. At the same time, you can intentionally confuse trainees by giving them tools they won't actually need during the exercise. For example, we give trainees the option of using the tool to convene a crisis meeting, even though according to the procedural procedures it will not make sense to use it. And if they use it? We can sent them an angry email from their boss or a hint that will explain that they used the tool wrongly. 

 We can then enhance the light instruction element by having a negative email or explanatory hint come in response to the activation. 





<div class="navigation" markdown>
  [&larr; Injects](../specify/injects.md){ .md-button }
  [Exercise specification &rarr;](../specify/exercise_specification.md){ .md-button }
</div>
<div class="navigation" markdown>
  [Specification Phase Overview](../specify/overview.md){ .md-button }
</div>
