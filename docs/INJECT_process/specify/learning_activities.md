In our case, we have created the following three LAs. 

LA1.1: Trainees must **conduct** an incident analysis. 

LA1.2: The trainees must **decide** whether the incident needs to be escalated. 

LA1.3: Trainees should **identify** the next appropriate course of action. 

### 2.
Now we have to decide **what they will specifically do in the platform within the given activities and what injects will make them do it.** These steps are closely linked, they cannot be separated. 
 
In the platform, trainees can generally perform the following actions:

- *Clicking the confirmation button after trainees have performed the LA* - this manifestation is great for LAs that don't have another clear manifestation in the platform, specifically: 
    - LA takes place primarily in the minds of the trainees - e.g. analysing documents, familiarising themselves with the situation, reading instructions. 
    - LA takes place outside the platform: trainees have to discuss something, present something, be interviewed by a journalist, etc. 

- *Establishing email communication* - trainees sent the first email to the given address. LA is filled just by sending the email. Note that for each unique address, this action is only recorded once by the platform. 

- *Email response from the instructor* - very often just sending an email will not be enough to show us that LA is fulfilled. In fact, the previous manifestation does not evaluate the content. In this case, the instructor reads the email and if the content is appropriate, he/she responds using a template that also indicates the fulfillment of the LA in the platform. 

- *Submitting an interactive inject* - trainees submit a questionnaire, scale or decision.

- *Response in free form inject with open response* - the manifestation is already a separate sending, we do not need to respond to the sent content further within the scenario.

- *Instructor response to free form inject with open response* - same principle as instructor email response.	

- *Trainees used a predefined tool.* Tools are a specific capability of the platform designer. We describe them in more detail in section called [Tools](../specify/tools.md)).	


But what is the most appropriate way of the platform manifestation? 
The answer to this question depends largely on your experience and intuition. It is true that the same LA can manifest in different ways, and it is always closely related to the inject options. The designer's task is to choose the one that makes sense in terms of the LO. Let's flesh this out further and work with the LA from the previous step. 

Note: **In the Specification Phase, you cannot proceed in a completely linear fashion**. The following examples may not make sense to you until you read the description of [Injects](../specify/injects.md).

#### Examples: 

a.	LA1.1: Trainees must conduct an incident analysis.

-	As exercise designers, we decided that the inject that would be associated with this LA would be to report the incident via email. However, this email will not contain all the necessary information. Therefore, we know that the logical outcome of a successful analysis will be to query the missing information. 
-	Thus, the most appropriate manifestation in the platform will be an email response from the instructor. Simply establishing email communication would not suffice as a relevant manifestation in the platform, we need to be sure that the trainee has asked for what they really need to know. 
-	Is there another way to do it? Yes, we could send a report to the trainees and let them confirm the analysis with a confirmation button. Or we could send trainees a Free form inject with an open response and the action in the platform would then be the instructor's response. 

b.	LA1.2: The trainees must decide whether the incident needs to be escalated.

-	Again, it depends on the injection. Fulfillment of this activity can be manifested, for example, by sending an interactive decision injection, an email response from the instructor, or by the instructor's response to a free form inject.

c.	LA1.3: Trainees should identify the next appropriate course of action.

-	Similarly, trainees here may receive an email from a supervisor as an inject, asking them for their next suggested steps. The manifestation can be just sending the email.  
-	Or they can get the same question in a free form injection with an open-ended answer, and the manifestation can also just be a simple submission. 

The above are just examples. Once you understand the principles, you can easily make a series of connections to fit your scenario. 

<div class="navigation" markdown>
  [&larr; Learning objectives](../specify/learning_objectives.md){ .md-button }
  [Injects &rarr;](../specify/injects.md){ .md-button }
</div>

<div class="navigation" markdown>
  [Specification Phase Overview](../specify/overview.md){ .md-button }
</div>