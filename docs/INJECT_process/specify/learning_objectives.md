# Learning Objectives

## In a nutshell
- Learning Objectives (LOs) specify what the trainee should learn or practice. 
- They must be set in a way that best responds to the needs for which the exercise was created. 
- LOs are the basic units that give structure to the whole exercise. Each LO is further decomposed into specific trainees actions - learning activities (LAs).
---

## Where are we in the INJECT process?

![](../../images/3.svg)

---

## Basic types of exercises 
There are two basic ways to approach the exercise. We discuss them in detail in Link: Section 5, but you should already know about them because it will allow you to think better about the LO of the whole exercise. 


### Type One: Strategic decision-making exercises

In this exercise, trainees are presented with individual problems in the form of free forms, questionnaires, scales, decision tasks, media inputs, etc. It is more suitable for more general scenarios or managerial positions, but can also be prepared for CSIRT members.


### The second type: process-technical exercises 
This exercise is based on an attempt to simulate the course of a process. The main input here is e.g. a document describing the response to incidents etc. Injections here are primarily via emails, it is possible to use abstraction of specific tools or measures and at the end there is a reflection part containing questionnaires or open questions. 

**Beware, if you don't have the actual organization and process as a basis, you will be in a very difficult situation as a designer.** In fact, if the organisation does not exist, you have to create it completely - a task that exceeds the contribution of TTX in its complexity. This is because the designer is at an impasse when trying to replicate the processes within the organisation, as every idea and conjecture has to be somehow explicitly communicated to the trainees in order for them to have a chance of successfully completing the exercise, but this means that instead of the TTX itself, the trainees spend much more time learning to understand this imaginary organisation. 
	
In addition, **you can also create your own type that combines the above options**, but if you are starting out, it is better to think about one of the two specific types of exercises. 

## How to specify Learning Objectives? 
### Specification of Learning Objectives by topic
Suppose we are designing an exercise for a public sector organization for which we have defined in the Understand phase the main need for the exercise as the ability to handle a cyber-attack involving a major leak of personal data. 

Depending on this, we have to ask the question: **What specifically should the involved employees handle in such an incident?** If I don't know the answers, I need to study best practices, talk to experts, or research available plans or processes. Let's assume that we have done all this and based on this we already define specific objectives for the exercise.  

Trainees should:
1.	Manage the incident in accordance with the process outlined in their manual. 
2.	Communicate clearly with all stakeholders. 
3.	Decide whether and how to communicate with the national data protection authority. 
4.	Suggest appropriate ways of dealing with the incident. 
5.	Reflect on the incident and learn from it. 

It is recommended to choose a **maximum of 7 LOs**. Remember that each LO will be further broken down into learning activities. 

### Specification of Learning Objectives as time phases
We primaly recommend to specify LOs by topic, because it allows you to make a very quick assessment of the success of each area at the end of the exercise and thus adjust the focus of the hot-wash at the end. 

However, there is also a second approach that specifies LOs as time phases. Suppose we still have the same need, but we want to emphasize the time and escalation factor more in the exercise. You can then take inspiration from the different phases of crisis management or we can have LO described simply as: 
	LO1: Preparation phase / Week before D-day
	LO2: Evaluating the first indications / Two days before D-Day
	LO3: Incident management / D-Day
	LO4: Final phase / Week after D-day

**The choice of approach to determine LO is not a technological choice, the implementation is always the same.** However, the choice of LO determination will **greatly influence how you think about the exercise or how easy it is to evaluate**. 

A fundamental requirement for success is that the LOs you set make sense in relation to why you are preparing the exercise. Anyway, please don't set anything now, familiarize yourself with the other sections first, it will make the whole job much easier. 


<div class="navigation" markdown>
  [&larr; Specification Phase Overview](../specify/overview.md){ .md-button }
  [Learning activities &rarr;](../specify/learning_activities.md){ .md-button }
</div>

<div class="navigation" markdown>
  [INJECT Process Overview](../intro/overview.md){ .md-button }
</div>


