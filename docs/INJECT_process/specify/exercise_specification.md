# Basic ways of exercise specification 

## In a nutshell
- This section builds on the knowledge from previous sections and the information gathered during the 01 Understanding Phase.
- It details how to specify exercises for the INJECT platform.
- We present two main approaches: strategic decision-making exercises and process-technical exercises.

---

## Where are we in the INJECT process?

![](../../images/3.svg)

---

## General information 
**Three basic elements of an exercise - learning objectives, learning activities and injects are closely related**. We found it useful to draw out each relationship on paper first so that we know - what are our learning objectives for the exercise (LO), what specifically the trainees will do in it (learning activities) and what injects we need to prepare for them. The whole design process is necessarily iterative. 

![](../../images/8.svg)

- You can combine the described [Injects](../specify/injects.md) in any way you like, but you may also find it useful to start from a predefined type and modify it further. 
As we indicated at the beginning of the section describing [Learning objectives](../specify/learning_objectives.md), you can approach the exercise in two basic ways:

- Type One: **strategic decision-making exercises** - In this exercise, trainees are presented with individual problems in the form of fre forms, questionnaires, scales, decision tasks, media inputs, etc. It is more suitable for more general scenarios or managerial positions, but can also be prepared for CSIRT members. 
- The second type: **process-technical exercises** - based on an attempt to simulate the course of a process. The main input is e.g. a document describing the response to incidents, etc. injects here are primarily via emails, it is possible to use abstraction of specific tools or measures and at the end there is a reflexive part containing questionnaires or open questions. **Beware, if you don't have the actual organization and process as a basis**, you will be in a very difficult situation as a designer. In fact, if the organisation does not exist, you have to create it completely - a task that exceeds the contribution of TTX in its complexity. 

??? "How to specify a strategic decision-making exercise"
      
    ### What do these exercises look like off the platform? 

      The trainees usually receive a paper assignment where the individual injects are presented, structured into phases or blocks. 
      The injects are most often in the form of text, but sometimes pictures are also added. Each inject is accompanied by questions. Depending on the type of target group, trainees work either in small teams or all together. Often a facilitator is involved.

    ### Modes of presentation in the platform:

      - It is recommended to divide trainees into smaller teams (3-5 members). It is best if everyone has a laptop with access to the platform, and they designate that only one of them will interact with it. Alternatively, they will divide their roles in some way
      - If a screen or larger screen is available, the exercise can be presented to a larger group (the discussion will be influenced by the dominant members). It is useful to have a facilitator
      - If the trainees cannot control the platform, one of the instructors can do so
      - The exercise can theoretically be run for one trainee. However, without discussion it becomes more of an interactive training. Each team can go at its own speed

    ### How to use different types of injects

      - Inject type: email - not used in this type of exercise
      - Inject type: execise information - suitable for instructions and outlining the general context at the beginning of the exercise
      - Injects of the type: document - strategic briefs, reports, etc. Consider sending before the exercise
      - Inject type: questionnaire / scale - recommended
      - Decision point - recommended
      - Inject type: free form - the main variant of inject for this type of exercise
      - Media injects - suitable for adding context or as a direct part of the script
      - Inject type: An off-platform activity - a spice up of the scenario, use is not necessary
      - Hint - as needed

    ### Use of tools 

      No tools are necessary for this type of exercise. If we want to use them, we recommend them for specific processes or actions that you want to highlight for the trainees.

      Examples: The tool can substitute a supervisor's decisions such as issuing a press release, conducting a legal analysis, contacting the police, etc.

    ### Possible scenario structures:

    #### A) Coherent story - individual injects are interconnected

      Everything relates to one storyline that unfolds gradually. Example:
      - The exercise starts with a document type inject - with a report from a national authority describing the current serious situation. This is followed by a combination of inject type:
      - Free form - activities: propose, argue, summarize...
      - Scale/questionnaire - evaluate, select, determine
      - The exercise is complemented first by injects in the social media channel that express the public's perception of the situation
      - The following is also article in major medium
      - The exercise proceeds to a serious decision - decision point type inject - we have an alternative conditional inject for each of the variants
      - Adding several injects to reflect on the actions taken (free form or questionnaire)
      - Exercise is otherwise more or less linear, hints do not need to be prepared in advance

    #### B) A sets of situations

      The trainees deals with different situations, which are not connected and are only briefly indicated
      - The exercise starts with a general introduction in exercise information
      - The first block follows with a description of the situation within the free form inject and a request for a description of the possible response
      - Followed by 2-3 interactive injects
      - The exercise continues in this way with a few more, tightened or slightly altered blocks
      - At the end there is space for more general reflection
      - The exercise can be improved by conditioning some of the responses in the free form injects

??? "How to specify a process-technical exercise"
    ### What do these exercises look like on the platform? 
      Trainees usually receive a paper assignment where individual injects are presented, structured into phases or blocks. The injects are most often in the form of text, but sometimes pictures are also added. Each inject is accompanied by questions. Trainees work in teams. It is assumed that the exercise relates to a process with which the trainees are familiar. 
    ### Modes of presentation in the platform:
      - It is recommended to divide trainees into smaller teams (3-5 members). It is best if each person has a laptop with access to the platform, and designates that only one of them will interact with it. 
      - The exercise can be completed even if only one of the trainees has a computer. 

    ### How to use different types of injects

      - Inject type: Email - the main type of inject for this type of exercise.  
      - Inject type: Execise information - intro inject, identity, tasks, context etc. 
      - Inject type: Document - politics, structures, manuals, guides
      - Inject type: questionnaire / scale - reflection, propability
      - Decision point - usually not used (decisions are made in an email communication)
      - Inject type: free form - gathering opinions or more speicific reflections. 
      - Medial injects - context, impact of the actions
      - Inject type: Hint - response to wrong action, action that wass missed or stuck in the exercise. 

    ### Use of tools 
      - For this type of exercise, the tools are absolutely essential. They usually try to mimic real tools that would be available to the trainee in a real situation and that could be used to resolve the incident. Most often these will be tools that are not too complex, such as IP blocking, network traffic dump, logging service logging, or creating a backup.
    ### Possible scenario structure:
      - Exercises most often start with an introductory inject in exercise information, which includes a description of the organisation concerned, the tasks of the exercisers and, if necessary, important contact details or documents to work with. 
      - The following 2 options are available, either the trainees will learn about the problem or incident, for example through a notification they receive by email, or they can be tasked to be proactive and use, for example, a system scanning tool to detect a problem in the system (a very technical exercise).
      - The tools that the trainee is tasked with using to resolve the incident or support the process play a significant role here. However, the whole scenario does not revolve only around the tools, but combines extensively with elements from the strategic decision-making exercises, where trainees are also guided through questionnaires, either in the form of scales or free form.
      - Many processes are also heavily based on communication with actors in the organization, which implies the possibility of abundantly involving communication via email with fictional characters (careful reduces automation and keeps the instructor more busy).
      - The exercise is very much based on the actions of the trainee and only if they perform the anticipated actions can they move towards the goal of resolving the incident.
      - Very often, hints are implemented to prevent the team from getting completely 
      - In the end, send reflective questionnaires

<div class="navigation" markdown>
 [&larr; Tools](../specify/tools.md){ .md-button }
 [Advanced approaches &rarr;](../specify/advanced_approaches.md){ .md-button }
</div>

<div class="navigation" markdown>
 [Specification Phase Overview](../specify/overview.md){ .md-button }
</div>