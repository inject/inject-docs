# Advanced Approaches 

## In a nutshell
- This section focuses on advanced approaches to exercise specification.
- These approaches can greatly enhance the overall trainee experience.
- Specifically, this includes the addition of tools, the use of conditioned responses, and the interaction between these elements.

---

## Where are we in the INJECT process?

![](../../images/3.svg)

---

## Use of Conditions 
Ordinary TTXs are usually unable to react in any way to the actions of trainees. IXP can do this on multiple levels. 
Some injects types have this integrated by default: 

-	The instructor chooses one of the possible answers to the email - the chosen answer can be followed up with the following automatic action of the platform..  
-	Using reactions to free form injects - similarly, diffrent reactions can be linked to the following automatic action of the platform.. 
-	Using decision points - each answer chosen can be associated with another automatic action of the platform. 
-	Hints - Hints are actually conditioned responses to trainees' behavior. 

However, the possibility of conditioning does not end there. Almost every action of the trainee that is manifested in the platform, can be linked by the another automatic action. This is done by so-called milestones. You can simply think of them as a switch. If the activity of the trainee in the platform activates this switch than the next automatic action happen. 

You can also set a condition that triggers the switch if a specific action doesn't occur by a set time. For example, if the CEO hasn't been informed by the 20th minute of the exercise, the platform will automatically send a complaint email asking why they were left out.

Similarly, a questionnaire may come in response or media coverage depending on the fact that trainees chose not to communicate a cyberattack on their organization. 

### Beware of two common problems:
**1.**	
You might think that it is possible to create a whole parallel storyline to the scenario, and you will be right. However, reality is very complex and the exercise scenario is always a simplification of it. Beware of the trap of developing a scenario into several parallel storylines. The reason is that you will ultimately spend much more time in preparation but the problem is, most of the content prepared in this way will not be seen by your trainees anyway (logically, they will choose only one of the storylines). Conditional injects are therefore an interesting and welcome option that conventional exercises do not offer. However, we recommend making these conditional storylines as minimalist as possible. Among other things, this will also come in handy during the exercise evaluation.

**2.**	
Linking of activities in the platforms is complex and needs to be verified several times that it cannot be bypassed. A concrete example might be a situation where a designer thinks like this: The final step in the exercise is the use of the tool "Convene Crisis Meeting". So we set the condition that after activating it, a questionnaire with a final reflection is automatically sent to the trainees. At first glance, everything looks logical, however, we have to ask ourselves - can the trainees use the tool earlier? For example, because of a bad evaluation of the situation - if so, it cannot be expected that this will always happen at the end of the exercise and therefore the condition set in this way is inappropriate. 

## Combinations of injects
*Please, note that this section will be further developed later. *

- The platform allows us to create very specific types of scenarios that can combine diffrent types of injects, eg. email communication with interactive injects. 
- Testing is essential when designing these types of exercises - because it is harder to know whether the intended links between activities will be understood by trainees. 

??? "Bonus: How to make exercise more fun?"

    ### To Be Done

    - gamification
    - offline aspects
    - emotions
    - strong story

<div class="navigation" markdown>
  [&larr; Exercise Specification](../specify/exercise_specification.md){ .md-button }
  
</div>
<div class="navigation" markdown>
  [Specification Phase Overview](../specify/overview.md){ .md-button }
</div>
