# UNDERSTAND: get all the information you need

## In a nutshell:

- This is the first phase of the INJECT process.
  Its goal is to understand everything needed for successful exercise preparation.
- It starts with an initial workshop to bring everyone together and get as much done as possible.
- We will give you specific recommendations on how to manage the entire phase.

---

## Where are we in the INJECT process?

![](../../images/2.svg)

---

## Initial workshop

Once the decision to hold a tabletop exercise is made, we propose conducting an initial workshop as soon as possible.

- Participants should include representatives from both the organizing team and the client's side.
- We recommend a set of actions and methods to structure the workshop effectively.
- The workshop will facilitate information sharing, establish collaboration, make decisions, and create a clear roadmap for the next steps.

TIP: Don't forget to appoint someone to moderate the workshop and someone to write down all the important details.

### 1. Discuss the exercise essentials

#### Clarify the basics

Once you've broken the ice, proceed straight to the essential facts about the exercise.
Follow a structured approach by employing the following list of five questions.

??? "5 questions"
    1. When should the exercise take place? Consider that the exercise date will impact subsequent actions, so it's essential to establish it early on.
    2. Identify the target audience of the exercise.
    3. Determine the main objective of the exercise.
    4. Decide where the exercise should be thematically positioned.
       Although this can be refined later, it's important to have an initial understanding.
       For instance, designing an exercise for decision-makers in a fictional scenario differs from one tailored for the CSIRT of a client's organization.
    5. Consider how the exercise should be structured.
       While detailed specifications will come later, begin gathering ideas on injects to include or preferred storyline.

#### Understand the genuine needs

Need is the reason why the client chooses to exercise.
**It's about connecting the exercise to what the client really wants to achieve**.
Usually, it's about protecting the organization's stuff, like money and reputation.
But by understanding the client's needs better, you might find out they want something more specific.
Like if their rival gets hit by a cyber attack, they want to make sure they're ready for the same thing.
Or maybe they want to learn from incidents that happened before.

*Example: our organization needs to properly handle data leakage incidents to avoid penalties and reputational risk that would lead to potential loss of profits.*

The organizational team must pinpoint the client's needs precisely.
**This way, we can make sure the exercise's learning objectives hit the mark**.
One good way to dig deeper is by using the 5 Whys method.

??? "5 Whys method"
    Let's kick off with a question: "Why is the exercise necessary?"

    - Jot down the answer, then ask "Why?" again.
    - Repeat this process, noting down each subsequent answer and asking "Why?" again, at least for the next four rounds.
    - After this, determine if you've uncovered any new insights that might impact the next exercise preparation.

#### Determine the preferred type of the exercise
  - **Type One: Strategic decision-making exercises**

  In this exercise, trainees are presented with individual problems in  the form of free forms, questionnaires, scales, decision tasks, media inputs, etc. It is more suitable for more general scenarios or managerial positions, but can also be prepared for CSIRT members.


  - **The second type: process-technical exercises**
  This exercise is based on an attempt to simulate the course of a process. The main input here is e.g. a document describing the response to incidents etc. Injections here are primarily via emails, it is possible to use abstraction of specific tools or measures and at the end there is a reflection part containing questionnaires or open questions. 


#### Determine the preferred setup for the exercise

Different setups offer unique requirements and possibilities that will influence next phases.
Here are the main options:

- **Teams with Single Laptops:**
  This setup is ideal for process-technical exercises where coordination and collaboration among team members are crucial.
  Each team member has their own laptop, allowing for individual work as well as seamless communication and coordination within the team.

- **One Big Screen for Team**:
  Alternatively, this setup is better suited for high-level strategic exercises that focus on policy and strategic decision-making.
  Instead of individual laptops, the team gathers around one big screen, enabling a centralized view of information and fostering discussion and collaboration among team members.

Variations of these setups can be explored to tailor the exercise experience to the needs.

### 2. Further Recommendations for Initial Workshop

Once you've discussed the essentials of the exercise, take advantage of having most of the key people in the room.
Focus on the following actions:

- **Brainstorm Inputs**:
  Identify and list all the inputs you will need from the client or third parties.
  This may include:

    - Email addresses to be used in the exercises
    - Relevant policies and documents
    - Pictures and logos etc.
    - Names of departments and key personnel (if relevant for intended scenario)
    - Any other specific information required by the scenario

- **Establish Cooperation Methods:**
  Determine your communication channels, methods for sharing information, and develop a roadmap for next steps and future meetings.

- **Assign Tasks:**
  Clarify the follow-up tasks that need to be completed.
  Assign these tasks to specific individuals with concrete deadlines.

- **Circulate Follow-Up:** After the meeting, circulate a follow-up summary to all attendees to ensure everyone is on the same page.

TIP: Be proactive and flexible.
It's not always possible to organize meetings with everyone present.
Don't wait for full attendance—test and discuss your work in shorter intervals with those available.

## Understanding phase after the workshop

After the initial workshop, the Understanding phase continues with the exercise designers diving deeper into the topic and exploring possibilities for the tabletop scenario.
The next steps are heavily influenced by the information gathered during the initial workshop.
Designers should leverage this data to refine their approach.

- **Research and Exploration:**
  Exercise designers thoroughly investigate the topic, exploring various angles and potential scenarios that align with the exercise objectives.
- **Regular Communication:**
  Maintain regular communication with the client to ensure the evolving scenario aligns with their needs.
  This ongoing dialogue helps shape the overall direction and ensures that the exercise remains relevant and targeted.

<div class="navigation" markdown>
  [&larr; INJECT Proces overview](../intro/overview.md){ .md-button }
  [2 Specification phase &rarr;](../specify/overview.md){ .md-button }
</div>
