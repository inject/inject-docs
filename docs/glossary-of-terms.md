# Glossary of terms

This section offers easy-to-understand explanations of important terms and concepts used in tabletop exercises and the INJECT Exercise Platform,
helping you navigate through exercise preparations and discussions with confidence.

## Basics

Tabletop Exercise, TTX
: a type of teaching activity designed to train professional teams in incident response to a crisis situation [[NIST](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-84.pdf)].
  The simulated crisis happens in the context of business operations in an organization, such as a phishing attack on employees or malware infecting the company infrastructure.
  The team members, who hold various roles in the organization (e.g., manager or cybersecurity incident responder) [[Angafor](https://doi.org/10.1108/ICS-05-2022-0085)], discuss which actions to take to effectively respond to the emergency while following proper protocols and regulations.
  These discussions are facilitated by instructors, who also present an exercise debriefing at the end.
  TTXs are an effective educational tool that enhance incident preparedness, particularly in communication, coordination, and collaboration [[Angafor](https://doi.org/10.1108/ICS-05-2022-0085)].

Inject
: a pre-scripted message, such as an email, provided to trainees during a tabletop exercise (TTX).
  Its purpose is to move the scenario forward and prompt additional actions.
  For example, an inject can inform the trainees about a data breach in their company, requiring them to respond accordingly.
  [[NIST](https://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-84.pdf)].

INJECT Exercise Platform, IXP
: The INJECT Exercise Platform (IXP) is an interactive web application designed to support the delivery and evaluation of TTXs [[Švábenský](https://is.muni.cz/publication/2390899/2024-ITiCSE-paper-platform-evolution-novel-learning-environment-tabletop-exercises-paper.pdf)].
  Designers use the platform to instantiate an exercise definition, which prescribes the exercise story, injects, available tools, and milestones.
  An exercise definition is implemented as a set of structured text-based files (in YAML format) that are both human- and machine-readable.
  IXP automates a substantial portion of the TTX by providing trainees with tools and inject response templates, significantly reducing the workload and personnel requirements for TTX delivery.

Exercise Definition
: This term refers to a complete scenario that has been incorporated into the required format for the platform, specifically using YAML files.
  Such a definition can be downloaded as a zip file and then launched as an exercise on the platform.

Hot Wash
: a post-exercise discussion or debriefing session where participants and facilitators provide immediate feedback and reflections on the exercise.
  It allows for real-time analysis and identification of strengths and areas for improvement.

Tool
: a simplified simulated version of a real-world computer application or service.
  Its purpose is to allow trainees to perform actions to respond to injects.
  For instance, instead of using an actual email client, trainees use an “email” tool to send and receive in-exercise messages.
  Another example is a “browser” tool that returns an in-exercise website based on the provided exercise URL.

Milestone
: a true/false condition that denotes whether a specific team reached a certain important situation in the TTX scenario.
  Its purpose is to track each team’s progress through the TTX.
  For example, it can mark that a team used an email tool to respond to a query from a manager.
  The exercise milestones can be completed in any order, and there is rarely a single correct solution, which makes team assessment challenging.

## Exercise subjects

Exercise Organizers
: These individuals are responsible for all aspects of preparation, including managing logistics and content preparation.
  They act as the project managers of the exercise.

Exercise Designers
: Their main role is to develop the exercise scenario.
  This includes defining the entire scenario and preparing the content for the platform.
  They are the authors of the exercise concept.

Exercise Instructors
: These individuals execute the exercise via the platform, provide briefings, and conduct the final hot wash.
  Unless specified otherwise, they also analyze the final data from the exercise.

Trainees
: These are the individuals who take part in the exercise.
  Their primary objective is to learn and gain experience from the exercise.

Exercise Provider
: This is the organization or department that oversees the entire process and appoints the exercise organizers.

Client
: the organization or part of the organization for which the exercise is prepared and executed.
  Ideally, the client understands the purpose and need for the exercise.

## Components of the platform

Trainee View
: It is designed for use during the Execution phase of the exercise.
  It provides trainees with access to the exercise scenario and injects, facilitating their participation and engagement.
  Through the Trainee View, participants can navigate the exercise, respond to injects, and collaborate with teammates in a simulated environment.

Instructor View
: It is a comprehensive interface used by exercise instructors during the Execution phase of the exercise.
  It allows instructors to manage the entire exercise, from overseeing participant interactions to facilitating discussions and providing guidance.
  With features for real-time monitoring and intervention, the Instructor View ensures smooth exercise execution and effective facilitation.

Analyst View
: It serves multiple purposes throughout the INJECT Process.
  During the Reflection phase, it is utilized for evaluation and data analysis, providing insights into participant performance and exercise outcomes.
  Additionally, the Analyst View serves as a source of information during the Understanding phase, offering valuable data and feedback to inform exercise preparation.
  Its intuitive interface enables thorough evaluation and informed decision-making for future exercises.

Editor
: INJECT Exercise Platform Component.
  It is a versatile tool used during the Definition and Preparation phases of the INJECT Process.
  It allows exercise designers to define learning objectives, create scenarios, and prepare exercise content.
  With its user-friendly interface, the Editor enables efficient scenario development and content organization. [More about Editor](INJECT_process/prepare/editor.md)