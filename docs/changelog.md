# Changelog

The INJECT Exercise Platform (IXP) is released in different versions.
Most significant changes between the releases are listed below.

This list is not complete, as each new release also includes numerous fixes
and smaller improvements.

## 3.0.0

### Improved UI

Improvements were made to the platform to create a better user experience and increase efficiency:

- improved home page
    - added ability to view non-running exercises
- improved Instructor View
    - new Overview page
    - new Team Overview page
    - improved team selector
    - new ability to change milestone states for all/selected teams
    - new ability to move in-exercise time
- improved Exercise Panel
    - new "info" button for definitions and exercises
- extended Analyst View
    - new learning objectives page
    - new questionnaires page

### New options for exercise definition

Additional optional features were added to the [exercise definition format](./tech/architecture/definitions/README.md)
to enable you to create more complex and engaging exercises.
The most important changes are listed below:

- ability to embed media (audio, video, and images) in injects
- support for more file types in document viewer (video, audio, and SVGs)
- optional confirmation buttons in injects
- open-ended questions
- ability to define multiple info channels
- added tools without parameters
- improved overlay inject UI

An exhaustive changelog can be found [here](./tech/architecture/definitions/CHANGELOG.md).

Furthermore, an interactive wizard called Editor was added for exercise definition.
You can find more information about it [here](INJECT_process/prepare/editor.md).

## 2.0.0

Changelog is not available for this version

## 1.0.0

Initial version of IXP
