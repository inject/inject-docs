<div class="container" markdown>
<div class="content" markdown>
<img class="ack_logo" src="../images/Logo_BW_inverz.png">
<div markdown>
This research is supported by the Open Calls for Security Research 2023–2029 (OPSEC) program granted by the Ministry of the Interior of the Czech Republic
under No. [VK01030007 – Intelligent Tools for Planning, Conducting, and Evaluating Tabletop Exercises](https://www.muni.cz/en/research/projects/69231).
</div>
</div>
</div>
