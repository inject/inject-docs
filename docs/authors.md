# Project contributors

| Name               | Role                                  |
| ------------------ | ------------------------------------- |
| Vít Baisa          | technical lead                        |
| Jindřich Burget    | frontend developer                    |
| Pavel Čeleda       | project manager                       |
| Roman Dvořák       | backend developer                     |
| Richard Glosner    | backend developer                     |
| Tomáš Hájek        | exercise designer                     |
| Martin Hofbauer    | devops, research assistant            |
| Martin Horák       | product manager                       |
| Martin Juhás       | backend lead developer                |
| Michal Krejčíř     | exercise designer                     |
| Adam Parák         | full-stack developer                  |
| Katarína Platková  | frontend developer                    |
| Valdemar Švábenský | researcher                            |
| Michal Urban       | lead developer of the first prototype |
| Marek Veselý       | full-stack developer                  |
| Jan Vykopal        | principal investigator                |
| Filip Šenk         | frontend developer                    |
