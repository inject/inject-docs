# Frequently Asked Questions

## Can I use the platform in multiple tabs?

Yes. Most navigation buttons contain a link, so you can easily open a new tab by clicking on them with the mouse wheel. However, please note that using multiple tabs may decrease the platform's performance for all users and some features, such as the read/unread states of injects, are not synchronized across tabs. Therefore, it is recommended to use this feature sparingly.

## How can I access error notifications after they've disappeared?

Error notifications are logged and can be accessed in the user settings. Furthermore, you can prevent a notification from disappearing by hovering over it with the cursor or by selecting it using the `tab` key.

## How do I know which documentation version matches my platform version?  
Check our [Version Compatibility](tech/version-compatibility.md) page that provides a clear overview of which documentation version corresponds to your platform version.